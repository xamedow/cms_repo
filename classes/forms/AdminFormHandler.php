<?php
namespace forms;

use main\Db;
use main\Router as r;
use main\FormHandler;
use main\Misc;

class AdminFormHandler extends FormHandler {
    private $exception_fields = array(
        'act' => '',
        'trash' => '',
        'id' => '',
        'deletable' => '',
        'session_hash' => ''
    );

    /**
     *  Совмещает {1}массив инициализации формы из массива опций ($opts['fields']), с {2}массивом элементов формы,
     *  сформированным из таблицы текущего модуля в БД. Исключает из {2} элементы из массива {3}exception_fields.
     *  Добавляет в значения {2} последние значения из БД (для опрделения максимального id, rank  и тд) и значения
     *  комментариев для данного поля из таблицы, для использования в качестве плейсхолдеров.
     *  Полученный совмещенный массив помещает в {1}.
     */
    // TODO parse for lesser functions.
    protected function setFields() {
        $table = 'mod_'.r::getPageName();
        $comments = Db::queryExec("SHOW full columns FROM $table WHERE comment!=''");
        $temp_arr = array();

        if( Misc::arrCheck($this->opts['fields']) && !empty($this->fields)) {

            if( is_array( current($this->fields) ) ) {
                foreach($this->fields as $field) {
                    $temp_arr = $temp_arr + $this->parseFields($field, $comments, true);
                }
            } else {
                $temp_arr = $this->parseFields($this->fields, $comments);
            }

            $this->opts['fields'] = array_merge($temp_arr, $this->opts['fields']);
        }
    }

    /**
     * @param array $field
     * @param       $comments
     * @param bool  $do_count - считать сколько раз была вызван метод.
     *
     * @return array
     */
    private function parseFields(array $field, $comments, $do_count = false) {
        $out = array();
        $field = array_diff_key($field, $this->exception_fields);
        $counter = '';
        if($do_count) {
            static $counter = 1;
        }
        foreach($field as $key => $val) {
            if( !array_key_exists($key, $this->opts['fields'])) {
                if($key === 'pass') {
                    $val = '';
                }
                $out[$key.$counter] = array('old_value'=>$val);
                foreach($comments as $comment) {
                    if(!empty($comment['Field']) && $comment['Field'] === $key) {
                        $out[$key]['placeholder'] = $comment['Comment'];
                    }
                }
            }
        }
        if ($do_count) {
            $counter++;
        }

        return $out;
    }
}