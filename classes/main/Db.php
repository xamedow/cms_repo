<?php

namespace main;

use main\ErrorHandler as e, main\FileHandler as fh;

class Db
{
    private static $dbh;

    /**
     * Выполняет инициализацию подключения к бд.
     * @param $creds
     * @throws \Exception
     * @throws \PDOException
     */
    public static function init($creds)
    {
        try {
            Misc::arrCheck($creds);
            extract($creds);
            self::$dbh = new \PDO("mysql:dbname=$dbname;host=$host;charset=utf8", $user, $pass);
            // Устанавливаем уровень выдачи ошибок.
            self::$dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            e::log('db_errors', $e, true);
        }
    }

    /** Выполняет запрос к бд, с массивом аргументов.
     * @param $query
     * @param array $args
     * @param bool $fetch
     * @return mixed
     * @throws \Exception
     */
    public static function queryExec($query, array $args = array(), $fetch = true)
    {
        if (isset(self::$dbh)) {
            $sth = self::$dbh->prepare($query);
            $sth->execute($args);
            if ($fetch) {
                $sth->setFetchMode(\PDO::FETCH_ASSOC);
                return $sth->fetchAll();
            }
        } else {
            throw new \Exception("Cannot connect to database.");
        }
    }


    /**
     * Получает все поля таблицы $table
     * @param $table
     *
     * @return mixed
     */
    public static function getTableFields($table) {
        return self::queryExec("SHOW columns FROM {$table}");
    }

    /**
     * Возвращает true, в случае если поля $fields присутствуют в таблице $table.
     * @param array $fields
     * @param $table
     *
     * @return bool
     */
    public static function fieldsInTable(array $fields, $table) {
        $table_fields = self::getTableFields($table);
        $found = false;
        foreach($fields as $field) {
            $found = false;
            foreach ($table_fields as $row) {
                if ($field === $row['Field']) {
                    $found = true;
                    break;
                }
            }
            if(!$found) break;
        }
        return $found;
    }
} 