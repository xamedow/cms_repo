<?php
/**
 * Created by PhpStorm.
 * User: itsme
 * Date: 06.10.2014
 * Time: 16:28
 */

namespace main;


/**
 *  Генератор постраничной навигации.
 * Class Paginator
 * @package main
 */
class Paginator
{
    /**
     * Текущая страница.
     * @var int
     */
    private $currentPage = 1;
    /**
     * Общее количество элементов.
     * @var int
     */
    private $items = 0;
    /**
     * Общее количество страниц.
     * @var int
     */
    private $pages = 1;
    /**
     * Таблица модуля.
     * @var null
     */
    private $table = null;
    /**
     * Количество элементов, выводимых на страницу.
     * @var int
     */
    private $visible = 1;
    /**
     * Стартовый индекс.
     * @var int
     */
    private $min = 0;

    public function __construct($table, $visible = 1) {
        $this->table = $table;
        $this->visible = $visible;
        $this->countItems();
        $this->setCurrentPage();
        $this->setPages();
        $this->setLimit();
    }

    /**
     * Устанавливает стартовый индекс для запроса.
     */
    private function setLimit() {
        if ($this->currentPage <= $this->pages && $this->currentPage > 0) {
            $this->min = ($this->currentPage - 1) * $this->visible;
        }
    }

    /**
     * Считает общее количество страниц.
     */
    private function setPages() {
        $this->pages = ceil($this->items / $this->visible);
    }

    /**
     * Считает общее количетсво элементов.
     * @throws \Exception
     */
    private function countItems() {
        $out = current( Db::queryExec("SELECT count(id) as count FROM {$this->table}") );
        $this->items = $out['count'];
    }

    /**
     * Устанаавливает из сессии текущую страницу.
     */
    private function setCurrentPage() {
        if(!empty($_SESSION[$this->table . '_page'])) {
            $this->currentPage = (float)$_SESSION[$this->table . '_page'];
        }
    }

    /**
     * Возвращает сформированную подстроку для запроса.
     * @return string
     */
    public function getLimit() {
        return " LIMIT {$this->min}, {$this->visible}";
    }

    /**
     * Возвращает строковое представление для текущей страницы.
     * @return string
     */
    private function getControls() {
        switch($this->currentPage) {
            case 1:
                return <<<HTML
                <li><span>первая страница</span>
                <li><a href="#" class="next">&gt;</a></li>
HTML;
            case $this->pages:
                return <<<HTML
                <li><a href="#" class="prev">&lt;</a></li>
                <li><span>последняя страница</span></li>
HTML;
            default:
                return <<<HTML
                <li><a href="#" class="prev">&lt;</a></li>
                <li><span>{$this->currentPage} страница из {$this->pages}</span></li>
                <li><a href="#" class="next">&gt</a></li>
HTML;
        }
    }

    /**
     * Возвращает html код пагинатора.
     * @return null|string
     */
    public function getPagesHTML() {
        $out = null;
        if($this->pages > 1 && $this->currentPage <= $this->pages) {
            $pages = $this->getControls();
            $out = <<<HTML
            <ul class="paginator" data-paginator-table="{$this->table}" data-cur-page="{$this->currentPage}">
                $pages
            </ul>
HTML;

        }
        return $out;
    }
}
