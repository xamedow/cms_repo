<?php

namespace main;

use main\Db as db, main\ErrorHandler as e, main\Misc as mi, main\Router as r;

/**
 * Класс фабрика для создания и управления формами.
 * Class FormHandler
 * @package main
 */
abstract class FormHandler
{
    private $variants = array(
        'select',
        'enum',
        'checkbox'
    );
    protected $transArr = array(
        'phone'=>'Телефон',
        'address'=>'Адрес',
        'city'=>'Город',
        'email'=>'Электронная почта',
    );
    protected static $count = 1;
    protected $fields = array();
    protected $opts = array();
    protected static $selectData = array();
    private $formView;
    private $elem;
    private $table;
    private $standartOpts = array(
        'class' => '',
        'id' => '',
        'fields' => array(),
        'method' => 'post',
        'action' => '',
        'enctype' => '',
        'label' => false,
        'ajax' => false
    );

    /**
     * @param $fields - список полей формы
     * @param array $opts - массив опций для формы
     * Спсиок опций
     * - class аттрибут формы
     * - method аттрибут формы
     * - action аттрибут формы
     * - enctype аттрибут формы
     * - label флаг, подключающий тэг label к каждому элементу
     * - ajax флаг, подключающий асихронную обработку
     * - fields массив, ключи которого траслируются в аттрибуты name, id, for. Подмассивы содержат аттрибуты
     * type, value(=placeholder, если placeholder не указан), placeholder, class
     * Пример:
     *
     * $opts = array( 'class'=>'new', 'action'=>'handler.php', 'ajax'=>true, 'fields'=>array(
     * 'name' => array('type'=>'text', 'value'=>'Андрей', 'placeholder'=>'Имя пользователя', 'class'=>'input_box')
     * ) );
     */
    protected function __construct($opts = array(), $fields)
    {
        $this->table = 'mod_'. r::getPageName();
        $this->fields = $fields;
        $this->opts = mi::arrCheck($opts);
        $this->filterOpts();
        $this->setFields();
        $this->setFormView();
    }

    /**
     * Устанавливает массив opts, содерщащий ключи только из массива standartOpts. Удаляет лишние и добавляет
     * отсутвующие из стандартных.
     * Пример:
     *
     * $this->opts = ['ajax'=>'true', 'inputs'=>'string']
     * FormHandler::filterOpts()
     * $this->opts = ['class' => '', 'fields' => array(), 'method' => 'post', 'action' => '', 'enctype' => '', 'ajax' => true]
     */
    private function filterOpts()
    {
        $this->opts = array_merge($this->standartOpts, array_intersect_key($this->opts, $this->standartOpts));
    }

    /**
     * Устанваливает свойству formView, HTML код заданной формы.
     */
    private function setFormView()
    {
        extract($this->opts);

        mi::arrCheck($fields);
        $class = $class ? " class='$class'" : '';
        $id = $id ? " id='$id'" : '';
        foreach ($fields as $name => $field) {
            $this->formView .= self::inputConstruct($name, mi::arrCheck($field), $label);
        }

        $enctype = $this->opts['enctype'] ? " enctype='{$this->opts['enctype']}'" : '';
        $this->formView = <<<HTML
        <form$class$id method='$method' action='$action'$enctype data-tb='{$this->table}'>
            {$this->formView}
        </form>
HTML;
    }

    /**
     * Возвращает сформированный HTML код элемента формы.
     * @param $name - имя элемента
     * @param $attrs - остальные аттрибуты элемента
     * @param $label - флаг для создания тэга label
     *
     * @return string - выходной HTML элемента
     */
// TODO refactor this!.
    private function inputConstruct($name, $attrs, $label)
    {
        if (strpos($name,'rank') !== false || strpos($name, 'class') !== false) {
            $attrs['type'] = 'hidden';
            $attrs['value'] = $attrs['old_value'];
        }
        if ($name === 'text') {
            $attrs['type'] = 'textarea';
            $attrs['value'] = !empty($attrs['value']) ? $attrs['value'] : $attrs['old_value'];
        }

        if(strpos($name, 'img') !== false && empty($attrs['value'])) {
            $attrs['type'] = 'file';
        }

        $type = $this->initAttr($attrs, 'type', 'text');
        if (empty($attrs['old_value'])) {
            $attrs['old_value'] = '';
        }
        $value = $this->initAttr($attrs, 'value', $attrs['old_value']);
        $class = $this->initAttr($attrs, 'class');
        $checked = $this->initAttr($attrs, 'checked');
        $placeholder = $this->initAttr($attrs, 'placeholder');
        $name = $name ? $name : 'input_' . rand(0, 100);
        $this->elem = array_key_exists('type', $attrs) ? $attrs['type'] : 'input';
        $count = self::$count;
        $this->elem_id = "{$this->elem}_{$name}_{$count}";

        switch ($this->elem) {
            case 'textarea':
                $input = "<textarea$class$placeholder id='$this->elem_id' name='$name'>{$attrs['value']}</textarea>";
                break;
            case 'checkbox':
                $input = "<input$class$type$value$placeholder$checked id='$this->elem_id' name='$name'>";
                break;
            case 'file':
                $this->opts['enctype'] = "multipart/form-data";
            default:
                $input = "<input$class$type$value$placeholder id='$this->elem_id' name='$name'>";
        }
        if( $this->elem === 'file' && !empty($attrs['old_value']) ) {
            $input = "
                    <span class='img_container'>
                        <a href='' class='delete_img' title='Удалить изображение'>
                            <img src='/modules/admin/assets/images/cancel.png'>
                        </a>
                        <img class='preview' id='{$this->opts['id']}' name='$name' src='/upload/".r::getPageName()."/sm{$attrs['old_value']}'>
                    </span>
                    ";
        }
        $chosen = $this->chooseConstruct($placeholder, $attrs['old_value'], $name);
        $input = $chosen ? $chosen : $input;
        if (isset($attrs['placeholder'])) {
            $attrs['placeholder'] = preg_replace('/\|[-&_\w]+$/i', '', $attrs['placeholder']);
        }
        $input = $label ? $this->labelConstruct("{$this->elem}_$name", $attrs) . $input : $input;
        ++self::$count;
        return $input;
    }

    /**
     * Выбирает соответствующий переданному аргументу метод и возвращает результат выполнения этого метода.
     * @param $placeholder
     * @param $value
     * @param $name
     *
     * @return bool
     */
    private function chooseConstruct($placeholder, $value, $name) {
        if($pos = strpos($placeholder, '|')) {
            $address = substr($placeholder, $pos + 1, -1);
            preg_match('/^(\w+)-?(\w+)?&?(\w+)?&?(\w+)?/i', $address, $matches);
            $length = count($matches);
            $matches = $length < 5 ? array_merge( $matches, array_fill($length, 5 - $length, '') ) : $matches;
            list(, $main, $table, $spec, $text) = $matches;
            $table = $table ? $table : $this->table;
            $this->elem = $main;

            $method = $main . ucfirst($spec) . 'Construct';
            if(method_exists($this, $method)) {
                return $this->$method($table, $value, $name, $text);
            }
        }

        return false;
    }

    private function parseParams($placeholder) {
        $out = array(
            'name' => null,
            'type' => null,
            'subtype' => null,
            'table' => null,
            'data_field' => null,
            'substitute' => null
        );
        //preg_match('/^(\w+)&?(\w+)?&?(\w+)?|?/i',$placeholder, $matches);
        return $placeholder;
    }

    /**
     * Возвращает сформированный HTML код элемента label, со значением ключа label либо placeholder массива $attrs.
     * @param $id
     * @param $attrs
     *
     * @return string
     */
    private function labelConstruct($id, $attrs)
    {
        $id .= '_' . self::$count;
        if (array_key_exists('label', $attrs) || array_key_exists('placeholder', $attrs)) {
            $name = array_key_exists('label', $attrs) ? $attrs['label'] : $attrs['placeholder'];
            return "<label for='$id'>$name</label>";
        }
        return '';
    }

    /**
     * Конструктор элемента checkbox.
     * @param $table
     * @param $value
     * @param $name
     *
     * @return string
     */
    private function checkboxConstruct($table, $value, $name) {
        $checked = $value ? ' checked="checked"' : '';
        $id = "checkbox_{$name}_" . self::$count;
        return <<<HTML
    <div class="checkbox">
        <input type='checkbox' value='1' name='$name' id='$id'$checked>
    </div>
HTML;
    }

    /**
     * Конструктор элемента select элементы - значения из типа enum.
     * @param $table
     * @param $value
     * @param $name
     *
     * @return string
     */
    private function selectEnumConstruct($table, $value, $name) {
        $data = current( Db::queryExec("SHOW columns FROM $table WHERE Field=:field", array(':field'=>$name)) );
        $options = '';

        if(strpos( $data['Type'], 'enum') !== false) {
            $enum = str_ireplace('\'', '', substr($data['Type'], 5, -1) );
            $enum = explode(',', $enum);
            foreach($enum as $val) {
                $selected = $value === $val ? ' selected="selected"' : '';
                $option = $this->transArr[$val];
                $options .= "<option value='$val'$selected>$option</option>";
            }
            $id = "select_{$name}_" . self::$count;
        }
        return empty($options) ? '' : "<select name='$name' id='$id'>$options</select>";
    }

    /**
     * Производит выборку данных для конструктора элемента select. Метод статический, чтобы кешировать запрос к БД.
     * @param string $table - таблица для выборки
     * @return array|mixed
     */
    private static function getSelectData($table) {
        if(empty(self::$selectData)) {
            $order = Db::fieldsInTable(array('parent', 'rank'), $table) ? " ORDER BY parent,rank" : '';
            self::$selectData = Db::queryExec("SELECT * FROM $table WHERE act=1$order");
        }
        return self::$selectData;
    }
    /**
     * Конструктор элемента select.
     * @param $table
     * @param $value
     * @param $name
     * @param $field
     *
     * @return string
     */
    private function selectConstruct($table, $value, $name, $field) {
        // TODO make this a distinct method
        $field = $field ? $field : 'name';
        preg_match('/[\d]+/i', $value, $match);
        $value = isset($match[0]) ? $match[0] : 0;
        $options = '';
        $data = self::getSelectData($table);
        if(!empty($data)) {
            foreach($data as $row) {
                if(isset($this->opts['fields']['rank']) && $row['rank'] == $this->opts['fields']['rank']['old_value']) {
                    continue;
                }
                $selected = $row['id'] == $value ? ' selected="selected"' : '';
                $options .= "<option value='{$row['id']}' $selected>{$row[$field]}</option>";
            }
            $options = "<option value='0'>нет значения</option>$options";
            $id = "select_{$name}_" . self::$count;
        }

        return empty($options) ? '' : "<select name='$name' id='$id'>$options</select>";
    }

    /**
     * Возвращает сформированный HTML код аттрибута $attr, со значением из массива $attrs, либо значением $default.
     * @param array $attrs
     * @param string $attr
     * @param string $default
     *
     * @return string
     */
    private function initAttr(array $attrs, $attr, $default = '')
    {
        return array_key_exists($attr, $attrs) ? " $attr = '{$attrs[$attr]}'" : " $attr = '$default'";
    }

    /**
     * Возвращает экземпляр соответстувующего дочернего класса.
     * @param $subClass - имя дочернего класса
     * @param $options
     * @param $fields
     *
     * @return mixed
     */
    static function getInstance($options, $subClass, $fields)
    {
        $classname = '\\forms\\' . ucfirst($subClass) . 'FormHandler';
        return new $classname($options, $fields);
    }


    /**
     *
     */

    abstract protected function setFields();

    /**
     * Возвращает сформированный HTML код формы.
     * @return mixed
     */
    public function getFormView()
    {
        return $this->formView;
    }

}