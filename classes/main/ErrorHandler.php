<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 05.05.14
 * Time: 17:23
 */

namespace main;

use main\FileHandler as fh;

class ErrorHandler
{

    public static function init()
    {
        set_error_handler( array('main\ErrorHandler', 'self::errorHandler') );
        register_shutdown_function( array('main\ErrorHandler', 'self::fatalErrorShutdownHandler') );
    }

    public static function log($file, $e, $trace = false)
    {
        $message = ($trace) ? $e->__toString() : $e->getMessage();
        fh::writeTo('logs/' . $file, "$message in {$e->getFile()} on line {$e->getLine()} " . date('d.m.Y H:i:s') . "\n\n");
    }

    public static function dummy()
    {
        return fh::readToString(DROOT . '/temp/index.html');
    }

    public static function errorHandler($code, $message, $file, $line)
    {
        $e = new \Exception($message, $code);
        self::log('fatal_errors', $e, true);
        echo self::dummy();
        die();
    }

    public static function fatalErrorShutdownHandler()
    {
        $last_error = error_get_last();
        if ($last_error['type'] === E_ERROR) {
            // fatal error
            self::errorHandler(E_ERROR, $last_error['message'], $last_error['file'], $last_error['line']);
        }
    }
} 