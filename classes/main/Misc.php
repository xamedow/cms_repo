<?php

namespace main;

class Misc
{
    private static $translit = array(
        ' ' => '_', '№' => '#',
        'а' => 'a', 'б' => 'b', 'в' => 'v',
        'г' => 'g', 'д' => 'd', 'е' => 'e',
        'ё' => 'yo', 'ж' => 'zh', 'з' => 'z',
        'и' => 'i', 'й' => 'j', 'к' => 'k',
        'л' => 'l', 'м' => 'm', 'н' => 'n',
        'о' => 'o', 'п' => 'p', 'р' => 'r',
        'с' => 's', 'т' => 't', 'у' => 'u',
        'ф' => 'f', 'х' => 'x', 'ц' => 'c',
        'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh',
        'ь' => "'", 'ы' => 'y', 'ъ' => "''",
        'э' => "e'", 'ю' => 'yu', 'я' => 'ya',

        'А' => 'A', 'Б' => 'B', 'В' => 'V',
        'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
        'Ё' => 'YO', 'Ж' => 'Zh', 'З' => 'Z',
        'И' => 'I', 'Й' => 'J', 'К' => 'K',
        'Л' => 'L', 'М' => 'M', 'Н' => 'N',
        'О' => 'O', 'П' => 'P', 'Р' => 'R',
        'С' => 'S', 'Т' => 'T', 'У' => 'U',
        'Ф' => 'F', 'Х' => 'X', 'Ц' => 'C',
        'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SHH',
        'Ь' => "'",  'Ы' => "Y'",   'Ъ' => "''",
        'Э' => "E'",   'Ю' => 'YU',  'Я' => 'YA',
    );
    /**
     * Возвращает массив пар ключ/значение из массива строк вида "ключ:значение"
     * @param $array
     * @return array
     */
    public static function get_param($array) {

        if(is_array($array)) {
            $out = array();
            foreach($array as $param) {
                $key = substr($param, 0, strpos($param, ':'));
                $val = trim(substr($param, strpos($param, ':') + 1));

                $out[$key] = $val;
            }

            return $out;
        } else {
            FileHandler::writeTo('logs/common_errors', $array.' - is not an array. ' . date('d.m.Y H:i:s') . "\n");
        }
    }

    /**
     * Возвращает значение параметра из конфигурационного файла *.json
     * @param $file_path - путь к файлу
     * @param $param_name - имя параметра
     * @return mixed - значение параметра
     * @throws \Exception
     */
    public static function getConfParam($file_path, $param_name) {
        $data = (array)json_decode( FileHandler::readToString($file_path) );
        if(array_key_exists($param_name, $data)) {
            return $data[$param_name];
        }
        throw new \Exception("Config param not found: $param_name");
    }

    /**
     * Парсим json строку в массив
     * @param $json_str
     * @return mixed
     * @throws \Exception
     */
    public static function readFromJson($json_str) {
        if(is_string($json_str)) {
            return (array)json_decode($json_str, true);
        }
        throw new \Exception('File is not a string: ' . $json_str);
    }

    /**
     * Gets transliterated version of string given.
     */
    public static function getTranslit($string, $direction = 'en') {
        if($direction === 'en') {
            return strtr( (string)$string, self::$translit );
        }
        return strtr( (string)$string, array_flip(self::$translit) );
    }


    public static function arrCheck($arr)
    {
        if (is_array($arr)) {
            return $arr;
        } else {
            throw new \Exception($arr . ' is not an array.');
        }
    }

    public static function getCallingClass($woNamespace = true) {
        //get the trace
        $trace = debug_backtrace();
        // Get the class that is asking for who awoke it
        $class = $trace[1]['class'];

        // +1 to i cos we have to account for calling this function
        for ( $i=1; $i<count( $trace ); $i++ ) {
            if ( isset( $trace[$i] ) ) // is it set?
                $caller = $trace[$i]['class'];
                if ( $class != $caller ) // is it a different class
                    return $woNamespace ? substr($caller, strpos($caller, '\\') + 1) : $caller;
        }
    }

    public static function checkEmptyRecursive($item) {
        if (is_array($item)) {
            $empty = TRUE;
            array_walk_recursive($item, function($item) use (&$empty) {
                $empty = $empty && empty($item);
            });
        } else {
            $empty = empty($item);
        }
        return $empty;
    }

}