<?php
namespace main;

class FileHandler
{

    /**
     *  Проверяет доступность пути $img_path к изображению. Если изображение недоступно, заменяет на fallback.
     * @param $img_path
     * @param string $fallback
     * @return string
     */
    public static function getImgSrc($img_path, $fallback = "/images/empty.jpg") {
        return empty($img_path) && is_string($img_path) && file_exists(DROOT. '/' . $img_path)
            ? $img_path
            : $fallback;
    }
    /**
     *  Записывает указанный текст в указанный файл.
     * @param $path
     * @param $text
     */
    public static function writeTo($path, $text)
    {
//        if (!file_exists($path)) {
//            touch($path);
//
        $path = self::setExtension($path);
        self::resolvePath($path, true);
        file_put_contents($path, $text, FILE_APPEND | LOCK_EX);
    }

    /**
     *  Записывает в темп файл строковое представление $msg (удобно для дебага ajax компонент)
     */
    public static function writeTemp() {
        $args = func_get_args();
        foreach ($args as $msg) {
            self::writeTo('logs/temp', var_export(self::setBreak($msg), true));
        }

    }

    /**
     * Добавляет указанное расширение к uri файла.
     * @param        $path
     * @param string $extension
     *
     * @return string
     */
    private static function setExtension($path, $extension = 'log') {
        if(!self::checkExtension($path)) {
            return "$path.$extension";
        }
        return $path;
    }
    /**
     * Проверяет, есть ли расширение в переданной строке uri.
     * @param $path
     *
     * @return bool
     */
    private static function checkExtension($path) {
        $pattern = '/\.[a-z0-9]{2,4}$/i';
        return (bool)preg_match($pattern, $path);
    }
    /**
     * Расставляет разрывы строк
     * @param $msg
     * @return string
     */
    private static function setBreak($msg) {
        if( !is_array($msg)) {
            return $msg . PHP_EOL;
        }
        return $msg;
    }
    /**
     * Читает файл, возвращает строку.
     * @param $file_path
     * @return string
     * @throws \Exception
     */
    public static function readToString($file_path)
    {
        if (file_exists($file_path)) {
            return file_get_contents($file_path);
        } else {
            throw new \Exception('File not found: ' . $file_path);
        }
    }

    /** Читает файл, возвращает массив строк.
     * @param $file_path
     * @return array
     * @throws \Exception
     */
    public static function readToArray($file_path)
    {
        if (file_exists($file_path)) {
            return file($file_path);
        } else {
            throw new \Exception('File not found: ' . $file_path);
        }
    }

    /**
     * Возвращает массив подпапок первого уровня вложенности.
     * @param $folder_path
     * @return array
     * @throws \Exception
     */
    public static function foldersToArray($folder_path)
    {
        $out = array();
        if ($handle = opendir($folder_path)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry !== '.' && $entry !== '..') {
                    $out[] = $entry;
                }
            }
            closedir($handle);
            return $out;
        } else {
            throw new \Exception('Folder not found: ' . $folder_path );
        }
    }

    /**.
     * Возвращает массив файлов первого уровня вложенности из указанной папки с указанным расширением.
     * @param $folder_path
     * @param $ext
     * @return array
     * @throws \Exception
     */
    public static function filesToArray($folder_path, $ext = '')
    {
        $out = array();
        if ($handle = opendir($folder_path)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry !== '.' && $entry !== '..' && (pathinfo(DROOT . "/$folder_path/$entry", PATHINFO_EXTENSION) === $ext || !$ext) ) {
                    $out[] = $entry;
                }
            }
            closedir($handle);
            return $out;
        } else {
            throw new \Exception('Folder not found: ' . $folder_path);
        }
    }

    /**
     * Проверяет аргумент(строка пути) на существование папок,
     * Если папка не обнаружена, флаг resolve указывает, создавать папку, либо генерировать исключение.
     * @param $path string
     * @param $resolve bool
     * @throws
     * @return bool
     */
    public static function resolvePath($path, $resolve = true)
    {
        // Убираем из пути папки до корня сайта (C:\domain\docs\).
        $chunks = self::getChunksFromPath($path);
        // Проверяем каждый элемент пути на существование.
        $out = DROOT;
        for ($i = 0; $i < count($chunks); $i++) {
            $out .= '/' . $chunks[$i];
            if ( $out !== DROOT && !self::checkExtension($out) && !file_exists($out)) {
                if ($resolve) mkdir($out);
                else throw new \Exception("Path - '$path' doesn't resolve properly.");
            }
        }
        return $out;
    }

    /**
     * Разбивает uri пути на массив, представляющий собой файловую структуру.
     * @param $path - путь к определяемому файлу.
     *
     * @return array
     */
    private static function getChunksFromPath($path) {
        return explode('/', self::clearDrootInPath($path));
    }

    /**
     * Удаляет корень сайта из пути к файлу.
     * @param $path
     *
     * @return string
     */
    private static function clearDrootInPath($path) {
        return strpos($path, DROOT) !== false ? substr((string)$path, strlen(DROOT) + 1) : $path;
    }
} 