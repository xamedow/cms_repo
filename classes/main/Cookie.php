<?php

namespace main;

class Cookie {

	/**
     * Читает указанный cookie
     * @param string $cookie название
     * @return bool
     */
    public static function read($cookie) {
        if ( !empty($_COOKIE[$cookie]) ) {
			return $_COOKIE[$cookie];
		} else {
			return false;
		}
    }
	
	/**
     * Проверяет наличие указанного cookie
     * @param string $cookie - название
     * @return bool
     */
    public static function is_set($cookie) {
        if ( !empty($_COOKIE[$cookie]) ) {
			return true;
		} else {
			return false;
		}
    }	
	
	/**
     * Записывает значение в cookie
	 * @param $cookie - название
	 * @param $value - значениеё
	 * @param $time - время жизни, по умолчанию 2 недели
	 * @param $path - путь, по умолчанию - корень сайта
     * @throws \Exception - Ошибка записи
     */
    public static function write($cookie,$value,$time=1209600,$path='/') {
        if ( !setcookie($cookie,$value,time()+$time,$path) ) throw new \Exception("Ошибка записи cookie - $cookie: $value");
    }
	
	/**
     * Удаляет cookie
	 * @param $cookie - название
     * @throws \Exception - Ошибка удаления
     */
    public static function delete($cookie) {
        if ( !setcookie($cookie,'',time()-3600) ) throw new \Exception("Ошибка удаления cookie - $cookie");
    }
} 