<?php
namespace main;

use main\FileHandler as fh, main\Cookie as c;

class Router
{
    /**
     * @var $urlParts - части урла, разобранные по /
     * @var $redirectUrl - урл переадресации
     * @var $pageName - текущая подстраница
     */
    private static $urlParts;
    private static $redirectUrl;
    private static $pageName;
    private static $sectionName;
    private static $mainModule;
    private static $notFound = true;
    private static $defaultPage = 'static';
    private static $checkPage = null;
    private static $subTable = array(
        'admin' => 'mod_users',
        'rest' => 'mod_users',
        'lk'    => 'ref_clients'
    );

    /**
     *
     * @param $urlParts - RURI константа (url)
     */
    public function __construct($urlParts)
    {
        self::setUrlParts($urlParts);
        self::parseUrlParts();
        self::setMainModule();
        self::setPageName();
        self::setRoutes();
        self::checkPageName();
        self::setDefaultPage();
        if (array_key_exists(self::$mainModule, self::$subTable)) {
            self::authRoutes(self::$mainModule, self::$defaultPage);
        }
    }

    /**
     * Аксессор
     * @param mixed $urlParts - части урла
     */
    public static function setUrlParts($urlParts)
    {
        self::$urlParts = $urlParts;
    }

    /**
     * Разбор урл по '/'
     */
    private static function parseUrlParts()
    {
        self::$urlParts = array_filter(explode("/", self::$urlParts));
    }

    /**
     * Устанавливает текущую страницу вида page.html, либо имя раздела модуля
     */
    private static function setPageName()
    {
            foreach (self::$urlParts as $url_part) {
                if (strpos($url_part, '.html') !== false) {
                    self::$pageName = substr($url_part, 0, -5);
                } else if ($url_part !== self::$mainModule) {
                    self::setSectionName($url_part);
                    self::$pageName = $url_part;
                }
            }
    }

    /**
     * Проверяет текущую страницу на пустоту, если пустая, ставит основную страницу.
     * @throws \Exception
     */
    private static function checkPageName () {

        self::$checkPage = Misc::getConfParam('conf/main.json', 'main_page');
        self::$pageName = !empty(self::$pageName)
            ? self::$pageName
            : self::$checkPage;

    }

    /**
     * Аксессор имени раздела модуля
     * @param $url_part
     */
    private static function setSectionName($url_part)
    {
        self::$sectionName = $url_part;
    }

    /**
     * Определяет контентный модуль, согласно URI запроса.
     */
    private static function setMainModule()
    {
        $modules = fh::foldersToArray(DROOT . '/modules');
        if (isset(self::$urlParts[1]) && ((strpos(self::$urlParts[1], '.html') === false) || self::$urlParts[1] === 'rest')) {
            foreach ($modules as $module) {
                if (self::$urlParts[1] === $module) {
                    self::$notFound = false;
                    self::$mainModule = $module;
                    break;
                }
            }
            self::checkNotFound();
        } else {
            $mainModule = Misc::getConfParam('conf/main.json', 'main_module');
            self::$mainModule = $mainModule ? $mainModule : 'static';
        }
    }

    /**
     * Проверяет на 404 ошибку
     */
    private static function checkNotFound() {
        if (self::$notFound) {
            self::sendNotFound();
        }
    }

    /**
     * Редирект на 404 страницу
     * @param string $url
     */
    public static function sendNotFound($url = '')
    {
        header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
        self::redirect($url);
    }

    /**
     *  Устанавливает дефолтную страницу для админки, выбирая первый активный модуль по рангу.
     */
    private static function setDefaultPage()
    {
        $page = current(Db::queryExec("SELECT name FROM modules WHERE act=1 ORDER BY rank LIMIT 1"));
        self::$defaultPage = $page['name'];

    }
    /**
     * Работа с таблицей маршрутов.
     */
    private static function setRoutes()
    {
        try {
            $routes = current(Db::queryExec('SELECT outcome FROM ref_routes WHERE income = :income',
                array(':income' => self::$pageName)));
        } catch (\PDOException $e) {
            ErrorHandler::log('db_errors', $e, true);
        }
        if (!empty($routes) && is_array($routes)) {
            self::redirect($routes['outcome']);
        }
    }

    /**
     * Маршруты для модулей с авторизацией. Главная
     * @param $module - модуль для которого подключается авторизация.
     * @param $def_page - страница перенаправления, в случае удачной авторизации.
     */
    private static function authRoutes($module, $def_page)
    {
        if (self::$mainModule === $module) {
            $sess = SessionHandler::getInstance();
            $sess->init();
            # Создаем экземпляр класса.
            $auth = Auth::getInstance(self::$subTable[ $module ]);
            # Если авторизованы и на странице авторизации, перенаправляем на дефолтную страницу.
            if ($auth->getAuth() && (self::$pageName === self::$checkPage)) {
				if ( !empty($_POST['remember']) ) c::write($module.'AuthCookie',$_SESSION[$auth->getUserId()],1209600);
                self::redirect("/$module/$def_page.html");
            } elseif (!$auth->getAuth() && self::$pageName !== self::$checkPage) {
                # Если не авторизованы, перенаправляем на страницу авторизации.
//                self::redirect("/$module/");
            } elseif (!$auth->getAuth()) {
                self::$pageName = '';
            }
        }
    }

    public static function sendHeader()
    {
        if (!headers_sent()) {
            foreach (func_get_args() as $header) {
                header($header);
            }
        }
    }

    /**
     *  Хелпер фильтраций урла
     */
    private static function checkUrl()
    {
        $filter = new Filter(self::$redirectUrl, 'url');
        self::$redirectUrl = $filter->apply();
    }

    /**
     * Обертка для редиректа на указанный url
     * @param $url - урл назначения
     */
    public static function redirect($url = '/')
    {
        self::$redirectUrl = "http://" . HHOST . $url;
        self::checkUrl();
        if (!headers_sent()) {
            header("Location: " . self::$redirectUrl);
        } else {
            exit("<html><head><meta  http-equiv='Refresh' content='0; URL=" . self::$redirectUrl . "'></head></html>");
        }
    }

    public static function getUrlParts() {
        return self::$urlParts;
    }

    /**
     * @return mixed
     */
    public static function getPageName()
    {
        return self::$pageName;
    }

    /**
     * @return mixed
     */
    public static function getMainModule()
    {
        return self::$mainModule;
    }

    /**
     * @return mixed
     */
    public static function getSectionName()
    {
        return self::$sectionName;
    }


}