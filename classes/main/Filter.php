<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 08.05.14
 * Time: 15:34
 */

namespace main;

class Filter {
    private $error;
    private $type;
    private $elem;
    private $types = array(
        'email',
        'url',
        'string',
        'encoded',
        'float'
    );

    function __construct($elem, $type)
    {
        $this->elem = $elem;
        $this->type = $type;
        if( !$this->isCapable() && !$this->checkType() ) {
            throw new \Exception($this->error);
        }
    }

    private function isCapable() {
        if(!extension_loaded('filter')) {
            $this->error = 'Filter extension was not loaded properly.';
            return false;
        }
        return true;
    }

    private function checkType() {
        if(!in_array($this->type, $this->types)) {
            $this->error = "Wrong filter type: '{$this->type}' was given";
            return false;
        }
        return true;
    }
    private function getFilter() {
        switch($this->type) {
            case 'email':
                $filter = FILTER_SANITIZE_EMAIL;
                break;
            case 'url':
                $filter = FILTER_SANITIZE_URL;
                break;
            case 'string':
                $filter = FILTER_SANITIZE_STRING;
                break;
            case 'encoded':
                $filter = FILTER_SANITIZE_ENCODED;
                break;
            case 'float':
                $filter = FILTER_SANITIZE_NUMBER_FLOAT;
                break;
            default:
                $filter = false;
        }
        return $filter;
    }
    public function apply() {
        $filter = $this->getFilter();
        $filter_func = is_array($this->elem) ? 'filter_var_array' : 'filter_var';

        return $filter_func($this->elem, $filter);

    }
} 