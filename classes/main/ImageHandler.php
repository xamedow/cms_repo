<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 25.06.14
 * Time: 11:45
 */

/**
 *  Класс изменения размеров изображений.
 * При создании экземпляра класса, передаем
 *      1 - абсолютный путь к оригинальному файлу
 *      2 - относительный путь к выходному файлу, без ведущего слеша и расширения
 *      3 - массив размеров выходного изображения (Ш х В)
 *
 * Пример: new ImageHandle($_FILE['name']['tmp_name'], 'upload/test', array(800, 600))
 * Class ImageHandle
 */
class ImageHandler
{
    private $error = ''; // Строка сообщений об ошибках.
    private $input; // Путь к оригинальному файлу.
    private $output; // Путь к выходному файлу.
    private $crop; // Флаг обрезки изображения.
    private $ext; // Расширение оригинального файла.
    private $in_dimensions = array(); // Массив с размерами оригинального изображения.
    private $out_dimensions = array(); // Массив с размерами выходного изображения.
    private $shifts = array(); // Массив сдвигов выходного изображения.


    /**
     *  Конструктор класса, проверяет есть ли необходимые функции в библиотеке gd, устанавливает расширение файла,
     * получает данные об изображении, проверяет на наличие ошибок.
     *
     * @param $input
     * @param $output
     * @param array $out_dimensions - массив выходных размеров изображения
     * @param $crop bool - флаг обрезки изображения
     */
    public function __construct($input, $output, array $out_dimensions, $crop = false)
    {
        $this->input = $input;
        $this->output = $output;
        $this->crop = $crop;
        $this->out_dimensions = $out_dimensions;
        $this->isCapable();
        $this->setInDimensions();
        $this->setExt();
        $this->checkExt();
        $this->throwE();
        if (!$this->checkDimensions()) {
            $this->compareOrientation();
            $this->setProportions();
            $this->getImage();
        }
    }

    /**
     * Проверка наличия необходимых модулей в библиотеке gd.
     */
    private function isCapable()
    {
        $info = gd_info();
        if (empty($info['JPEG Support']) && empty($info['PNG Support'])) {
            $this->error = "Installed GD library doesn't support necessary file types.";
        }
    }

    /**
     * Определяет размеры переданного файла, его MIME тип и тд.
     */
    private function setInDimensions()
    {
        $this->in_dimensions = getimagesize($this->input);
    }

    /**
     *  Проверяет соответсвие расширений файлов изображений, расширению переданного файла.
     * @return bool
     */
    private function checkExt()
    {
        $imageExt = array('jpg', 'jpeg', 'png');
        if (in_array($this->ext, $imageExt)) {
            $this->output = $this->output . '.' . $this->ext;

            return true;
        }
        $this->error = "Input file is not an acceptable image.";

        return false;
    }

    /**
     * Определеяет расширение переданного файла.
     */
    private function setExt()
    {
        $ext = strtolower(substr($this->in_dimensions['mime'], strpos($this->in_dimensions['mime'], '/') + 1));
        $this->ext = $ext === 'jpg' ? 'jpeg' : $ext;
    }

    /**
     *  Устанавливает ориентацию изображения.
     */
    private function getOrientation($info)
    {
        if ($info[0] > $info[1]) {
            $orientation = 'landscape';
        } elseif ($info[0] < $info[1]) {
            $orientation = 'portrait';
        } else {
            $orientation = 'square';
        }

        return $orientation;
    }

    /**
     * Сравнивает ориентации оригинала и выходного изображения, в случае если они не равны,
     * меняет местами размеры выходного изображения
     */
    private function compareOrientation()
    {
        $in_orient = $this->getOrientation($this->in_dimensions);
        $out_orient = $this->getOrientation($this->out_dimensions);

        if ($in_orient === 'square') {
            //TODO set action
        } elseif ($in_orient !== $out_orient) {
            $this->out_dimensions[0] = $this->out_dimensions[0] + $this->out_dimensions[1];
            $this->out_dimensions[1] = $this->out_dimensions[0] - $this->out_dimensions[1];
            $this->out_dimensions[0] = $this->out_dimensions[0] - $this->out_dimensions[1];
        }
    }

    /**
     *  При указании одновременно ширины И высоты, больших, чем оригинал, сохраняет оригинал.
     */
    private function checkDimensions()
    {
        if ($this->out_dimensions[0] >= $this->in_dimensions[0] && $this->out_dimensions[0] >= $this->in_dimensions[0]) {
            move_uploaded_file($this->input, $this->output);

            return true;
        }

        return false;
    }

    private function setProportions() {
        list($in_width, $in_height) = $this->in_dimensions;
        list($width, $height) = $this->out_dimensions;
        $in_ratio = $in_width / $in_height;

        if ($width/$height > $in_ratio) {
            $this->out_dimensions[0] = $height*$in_ratio;
        } else {
            $this->out_dimensions[1] = $width/$in_ratio;
        }
    }

    private function getImage()
    {
        $create_func = 'imagecreatefrom' . $this->ext;
        $image_func = 'image' . $this->ext;

        list($in_width, $in_height) = $this->in_dimensions;
        list($width, $height) = $this->out_dimensions;
        $in_handle = $create_func($this->input);
        $out_handle = imagecreatetruecolor($width, $height);

        if ($this->ext === 'png') {
            $transparent = imagecolorallocatealpha($out_handle, 0, 0, 0, 127);
            imagefill($out_handle, 0, 0, $transparent);
            imagecopyresampled($out_handle, $in_handle, 0, 0, 0, 0, $width, $height, $in_width, $in_height);
            imagealphablending($out_handle, false);
            imagesavealpha($out_handle, true);
        } else {
            imagecopyresampled($out_handle, $in_handle, 0, 0, 0, 0, $width, $height, $in_width, $in_height);
        }
        // TODO check for errors.
        if(function_exists($create_func)) {
            $image_func($out_handle, $this->output);
        }
        imagedestroy($in_handle);
        imagedestroy($out_handle);
    }

    /**
     *  Выбрасывает исключение при наличии ошибок.
     * @throws Exception
     */
    private function throwE()
    {
        if ($this->error) {
            throw new \Exception($this->error);
        }
    }

    /**
     * @return mixed
     */
    public function getExt()
    {
        return $this->ext;
    }


}