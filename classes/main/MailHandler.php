<?php
/**
 * Created by PhpStorm.
 * User: itsme
 * Date: 13.10.2014
 * Time: 10:08
 */

namespace main;
use modules\Contact;

/**
 * Класс хелпер, для отправки почты.
 * Class MailHandler
 * @package main
 */
class MailHandler {
    private $to;
    private $from;
    private $subject;
    private $body;
    private $headers;

    /**
     * @param $subject - Тема письма.
     * @param bool $to - Получатель, если не указан, то первый из контактных данных сайта.
     * @param bool $from - Отправитель, если не указан, то первый из контактных данных сайта.
     * @param bool $body - тело письма.
     * @throws \Exception
     */
    public function __construct($subject, $to = false, $from = false, $body = false) {
        $mail = new Contact('email');
        $this->from = $from ? $from : $mail->getView();
        $this->to = $to ? $to : $mail->getView();
        $this->subject = $subject;
        $this->body = $body;
    }

    public function send() {
        $this->setHeaders();
        $this->setBody();
        mail($this->to, $this->subject, $this->body, $this->headers);
    }

    private function setBody() {
        $this->body = <<<HTML
    <h2>{$this->subject}</h2>
    {$this->body}
HTML;
    }

    private function setHeaders() {
        $headers = "From: {$this->from}\r\n";
        $headers.= 'MIME-Version: 1.0' . "\r\n";
        $headers.= 'Content-type: text/html;charset=utf-8' . "\r\n";
        $this->headers = $headers;
    }
} 