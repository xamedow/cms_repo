<?php
namespace main;
use main\Misc as mi;

class ListHandler
{
    protected $data_arr = array();
    private $page_name = '';
    private $module;
    private $default_extension;

    /**
     * @param string $page_name
     * @param string $module
     * @param string $default_extension
     */
    public function __construct($page_name, $module = 'static', $default_extension = 'html')
    {
        $this->page_name = $page_name;
        $this->module = $module;
        $this->default_extension = $default_extension;
    }

    protected function getHref($page_name)
    {
        $page = empty($page_name) ? '' : $page_name . '.html';
        return "http://" . HHOST . "/{$this->module}/$page";
    }

    /**
     * @param array $data_arr
     */
    protected function setDataArr($data_arr)
    {
        $this->data_arr = $data_arr;
    }

    /**
     * @return array
     */
    protected function getDataArr()
    {
        return $this->data_arr;
    }

    private function makeLiHtml ($href, $name, $elem, $class, $admin) {
        if($admin) {
            return <<<HTML
                            <li$class>
                                <a$href title="$name">
                                    <i class='fa fa-tachometer fa-fw'>
                                        <div class='icon-bg bg-orange'></div>
                                    </i>
                                    <span class='menu-title'>$name</span>
                                </a>
                            </li>
HTML;

        }
        return "<li><$elem$class$href>$name</$elem></li>";
    }
    /**
     * @return bool|string
     */
    protected function getLi($nest = 0, $is_admin = false)
    {
        $data_arr = mi::arrCheck($this->data_arr);
        $list_items = '';
        $class = $nest ? ' class=indented' : null;
        foreach ($data_arr as $row) {
            if (is_array($row)) {
                $page_name = preg_match('/[a-z]*\.html/i', $row['page_name'])
                    ? $row['page_name']
                    : strtolower(mi::getTranslit($row['page_name']));
                $href = ' href="' . $this->getHref($page_name) . '"';
                $name = $row['name'];
                $parent = (empty($row['parent'])) ? '0' : $row['parent'];
                $elem = null;

                if ($nest == $parent) {
                    $class = null;
                    if ($page_name === $this->page_name && $row['module_name'] === $this->module) {
                        $class .= $class ? ' active' : ' class=active';
                        $elem = 'span';
                    } else {
                        $elem = 'a';
                    }
                    $list_items .= $this->makeLiHtml($href, $name, $elem, $class, $is_admin);
                    $list_items .= $this->getLi($row['id'], $is_admin);
                }
            } else {
                continue;
            }
        }
        return $list_items;
    }
}

class nestedListhandler extends ListHandler
{
    public function get_li($nest_level = 0, $module = '')
    {
        if (!$module) {
            $module = parent::getModule();
        }
        $data_arr = parent::getDataArr();
        if (is_array($data_arr)) {
            $list_items = '';

            foreach ($data_arr as $row) {
                if (
                    is_array($row) &&
                    (!empty($row['anchor']) || !empty($row['redirect'])) &&
                    $row['parent'] == $nest_level
                ) {
                    if ($row['anchor'] === parent::getPageName()) {
                        $list_items .= "<li><span>{$row['name']}</span></li>";
                    } else {
                        $redirect = (!empty($row['redirect'])) ? $row['redirect'] : '';
                        $href = parent::getHref($redirect, $row['anchor'], $module, $this->getDefaultExtension());

                        $nested_ul = $this->get_li($row['id'], $redirect);
                        $nested_ul = $nested_ul ? "<ul class='nested'>$nested_ul</ul>" : '';

                        $list_items .= "<li><a href=\"$href\">{$row['name']}</a>$nested_ul</li>";
                    }
                } else {
                    continue;
                }
            }

            return $list_items;
        }

        return false;
    }
}

class textList extends ListHandler
{

    protected $data_arr;

    public function __construct($data_arr)
    {
        $this->data_arr = $data_arr;
    }

    public function get_li()
    {
        if (is_array($this->data_arr)) {
            $list_items = '';

            foreach ($this->data_arr as $row) {
                if (is_array($row)) {
                    $name = (!empty($row['name'])) ? $row['name'] : current($row);
                    $list_items .= "<li>$name</li>";
                } else {
                    continue;
                }
            }

            return $list_items;
        }

        return false;
    }
}

class nestedText extends nestedListhandler
{
    public function get_li($nest_level = 0, $module = '')
    {
        $data_arr = parent::getDataArr();
        if (is_array($data_arr)) {
            $list_items = '';

            foreach ($data_arr as $row) {
                if (
                    is_array($row) &&
                    $row['parent'] == $nest_level
                ) {
                    $redirect = (!empty($row['redirect'])) ? $row['redirect'] : '';
                    $parent = ($row['parent']) ? $row['parent'] : 0;
                    $id = ($row['id']);
                    $nested_ul = '';

                    if (!$parent) {
                        $nested_ul = $this->get_li($id, $redirect);
                        if ($nested_ul) {
                            $nested_ul = "<ul class='nested'>$nested_ul</ul>";
                        }
                    }

                    $list_items .= "<li>{$row['name']}$nested_ul</li>";
                } else {
                    continue;
                }
            }

            return $list_items;
        }

        return false;
    }
}

class anchorList extends ListHandler
{
    private $with_year = false;

    public function __construct($data_arr, $page_name, $default_module = 'static', $default_extension = 'html', $with_date = false, $with_year = false)
    {
        parent::__construct($data_arr, $page_name, $default_module, $default_extension);
        $this->with_date = $with_date;
        $this->with_year = $with_year;
    }

    public function get_li()
    {
        $data_arr = parent::getDataArr();
        if (is_array($data_arr)) {
            $list_items = '';

            foreach ($data_arr as $row) {
                if ($row) {
                    $module = parent::getModule();
                    $href = parent::getHref('', $row['anchor'], $module, $this->getDefaultExtension());
                    $name = $this->truncate($row['name']);
                    $date = ($this->with_date) ? $this->get_date_block($row['date'], $this->with_year) : '';

                    $list_items .= "<li>$date<a href=\"$href\">$name</a></li>";
                }
            }

            return $list_items;
        }
        return false;
    }

    // Helpers.

    private function get_date_block($date, $with_year)
    {
        if (is_string($date)) {
            $date = new DateTime($date);
            $ru_month = array(
                1 => 'января',
                2 => 'февраля',
                3 => 'марта',
                4 => 'апреля',
                5 => 'мая',
                6 => 'июня',
                7 => 'июля',
                8 => 'августа',
                9 => 'сентября',
                10 => 'октября',
                11 => 'ноября',
                12 => 'декабря',
            );
            $year = ($with_year) ? "<span>{$date->format('Y')}</span>" : '';

            return <<<EOD
            <div class="date">
                {$date->format('j')}
                <span>{$ru_month[$date->format('n')]}</span>
                $year
            </div>
EOD;
        }
        return false;
    }

    private function truncate($string)
    {
        $max_length = 100;
        if (is_string($string)) {
            return (strlen($string) > $max_length) ? substr($string, 0, $max_length) . '&hellip;' : $string;
        }
        return false;
    }
}