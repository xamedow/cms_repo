<?php
/**
 * Created by PhpStorm.
 * User: itsme
 * Date: 01.10.2014
 * Time: 11:33
 */

namespace main;


class SessionHandler {

    private $data;
    private static $instance;
    private function __costruct() {

    }
    public function init() {
        if(!defined('SID')) {
            session_start();
        }
    }

    public static function getInstance() {
        if(empty(self::$instance)) {
            self::$instance = new SessionHandler();
        }
        return self::$instance;
    }

} 