<?php
namespace modules;

use main\Db;

class Hq implements ModuleInterface {
    private $user;
    private $options;

    public function __construct($options) {
        $this->options = $options;
        $this->setUser();
    }

    private function setUser() {
        if( !empty($_SESSION['mod_users_id']) ) {
            $this->user = current(Db::queryExec("SELECT name FROM mod_users WHERE id = :id", array(':id'=>$_SESSION['mod_users_id'])));
        }
    }

    public function getView() {
        $host = HHOST;
        return <<<HTML
        <div id="hq-action-select" class="customselect"  tabindex="1">
            <div id="user" class="customselect-title">
                {$this->user['name']}
                <ol class="customselect-options">
                    <li>Перейти на сайт $host</li>
                    <li>Сменить пароль</li>
                    <li>Выход</li>
                </ol>
            </div>
        </div>
HTML;

    }
} 