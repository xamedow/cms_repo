<?php
/**
 * Created by PhpStorm.
 * User: itsme
 * Date: 01.10.2014
 * Time: 9:22
 */

namespace modules;


use main\Router;
use main\SessionHandler;

class Captcha extends Module implements ModuleInterface {
    private $opts = array(
        'width' => 150,
        'height' => 50,
        'numbers' => 5,
        'bg'=>array(255, 255, 255)
    );
    private $path;
    private $code;
    private $img;
    private $coords = array();

        // Символы, используемые в коде
    private $symbols = array('1','2','3','4','5','6',
    '7','8','9','0');
        // Компоненты для RGB-цвета
    private $components = array('50','70','90','110',
    '130','150','170','190','210');
    private $componentsCount;

    public function getView()
    {
        $this->drawCode();
        $this->pushCode();
        $path = "upload/lk/" . rand(0, 100) . ".jpg";
        $this->path = DROOT . '/' . $path;
        $http_path = 'http://' . HHOST . '/'. $path;
        imagejpeg($this->img, $this->path);

        return "<img src='{$http_path}' alt='captcha'>";
    }

    private function drawCode() {
        for($i = 0; $i < $this->opts['numbers']; ++$i) {
            $symbol = $this->getSymbol();
            $this->setImg();
            $this->setCode($symbol);
            $this->setCoords();
            list($x, $y) = $this->coords;
            imagestring($this->img, 6 ,$x, $y, $symbol, $this->getColor());
        }
    }

    private function getSymbol() {
        return $this->symbols[rand( 0, count($this->symbols) - 1)];
    }

    private function setImg() {
        if(empty($this->img)) {
            $this->img = imagecreatetruecolor($this->opts['width'], $this->opts['height']);
            $bg = imagecolorallocate($this->img, 255, 255, 255);
            imagefill($this->img, 0, 0, $bg);
        }
    }

    private function setCode($symbol) {
        $this->code .= $symbol;
    }

    private function setCoords() {
        $this->coords[0] = empty($this->coords[0])
            ? $this->opts['width'] * 0.08
            : $this->coords[0] = $this->coords[0] +
                ($this->opts['width'] * 0.8) / $this->opts['numbers'] + rand(0, $this->opts['width'] * 0.01);

        $this->coords[1] = rand(0,1)
            ? (($this->opts['height'] * 1) / 4) + rand(0, $this->opts['height'] * 0.1)
            : (($this->opts['height'] * 1) / 4) - rand(0, $this->opts['height'] * 0.1);

    }

    private function getColor() {
        $this->setComponentsCount();
        return imagecolorallocatealpha(
            $this->getImg(),
            $this->components[rand(0, $this->componentsCount)],
            $this->components[rand(0, $this->componentsCount)],
            $this->components[rand(0, $this->componentsCount)],
            rand(10, 30));
    }

    private function setComponentsCount() {
        if(empty($this->componentsCount)) {
            $this->componentsCount = count($this->components) - 1;
        }
    }

    private function getImg() {
        $this->setImg();
        return $this->img;
    }

    private function pushCode() {
        $_SESSION['captcha'] = $this->code;
    }
} 