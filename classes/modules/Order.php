<?php
/**
 * Created by PhpStorm.
 * User: itsme
 * Date: 08.10.2014
 * Time: 12:04
 */

namespace modules;


use main\Db;

class Order extends Module {
    private $indices;

    public function getView() {
        if (isset($_POST['action']) && method_exists($this, $_POST['action'])) {
            if (!headers_sent()) {
                header('Content-Type: application/json');
            }
            $args = empty($_POST['args']) ? null : array($_POST['args']);
            echo json_encode(call_user_func(array($this, $_POST['action']), $args));
            die();
        }
    }

    private function removeProduct() {
        $id = func_get_args();
        $id = $id[0][0];
        if(isset($_SESSION['basket'])) {
            unset($_SESSION['basket'][$id]);
        }
        return null;

    }
    private function getProducts() {
        $data = $this->getProductsData();
        return $data;
    }
    private function getBasket() {
        if(isset($_SESSION['basket'])) {
            return $_SESSION['basket'];
        }
        return null;
    }

    private function getProductsData() {
        $this->getProductsIndices();
        if($this->indices) {
            return Db::queryExec("SELECT id, name, measure_in, price FROM mod_catalog WHERE id IN ({$this->indices})");
        }
        return $this->indices;
    }
    private function getProductsIndices() {
        $this->indices = is_array($_SESSION['basket'])
            ? implode(',', array_keys($_SESSION['basket']))
            : null;
    }
} 