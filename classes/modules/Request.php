<?php
/**
 * Date: 09.05.14
 * Time: 21:19
 */

namespace modules;
use main\Filter, main\FileHandler as fh, main\MailHandler;

/**
 * Модуль обработки форм заявок на обратный звонок.
 * Class Request
 * @package modules
 */
class Request extends Module {
    private $data;
    private $transArr = array(
        'phone'=>'Телефон',
        'name'=>'Имя',
        'mail'=>'Электронная почта'
    );

    private function setData() {
        $fpost = new Filter($_POST, 'string');
        $this->data = $fpost->apply();
    }

    private function getBody() {
        $body = '';
        if(!empty($this->data)) {
            foreach($this->data as $key => $val) {
                if($key !== 'request') {
                    $body .= <<<HTML
            <tr>
                <td>{$this->transArr[$key]}</td>
                <td>$val</td>
            </tr>
HTML;
                }
            }
            $body = "<table>$body</table>";
        }
        return $body;
    }

    public function getView() {
        if (!isset($_POST['request'])) {
            return $this->options;
        }
        $this->setData();
        $mailer = new MailHandler("Запрос на обратный звонок", false, 'info@' . HHOST, $this->getBody());
        $mailer->send();

        return '<div class="request_text">Ваша заявка отправлена.</div>';
    }
}