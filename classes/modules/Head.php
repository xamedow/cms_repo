<?php
namespace modules;

use \main\Router as r, \structure\Model as m;

class Head extends Module
{
    public function getView()
    {
# Интсанциируем, провайдим имя и тип модуля
        $mod_head = new \structure\Controller('head', 'secondary');

# Подключаем модель, получая из нее переменные $meta_data, $styles и $scripts.
        $meta_data = array('title' => 'Панель управления сайтом ' . HHOST, 'keywords' => '', 'description' => '');
        if (r::getMainModule() !== 'admin') {
            $head_model = new m('head', 'secondary', r::getPageName());
            $meta_data = $head_model->getMeta();
        }

        $scripts = m::getScripts();
        $styles = m::getStyles();

# Считываем представление.

        if( !empty($this->options) ) {
            $mod_head->setView($this->options);
        } else {
            $mod_head->readView();
        }

# Обрабатываем представление.
        $mod_head->replaceView('title', $meta_data['title'], false);
        $mod_head->replaceView('keys', $meta_data['keywords'], false);
        $mod_head->replaceView('description', $meta_data['description'], false);
        $mod_head->replaceView('styles', $styles, false);
        $mod_head->replaceView('scripts', $scripts, false);

# Отдаем сформированное представление.
        return $mod_head->getView();
    }
} 