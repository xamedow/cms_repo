<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 09.05.14
 * Time: 21:19
 */

namespace modules;


use main\Db;

class Banner implements ModuleInterface {
    private $options;
    private $data;

    public function __construct($options) {
        $this->options = trim($options);
        $this->setData();
    }

    private function setData() {
        $this->data = current(Db::queryExec("SELECT text FROM mod_banner WHERE name=:name", array(':name'=>$this->options)));
    }

    public function getView() {
        return empty($this->data['text']) ? '' : $this->data['text'];
    }
} 