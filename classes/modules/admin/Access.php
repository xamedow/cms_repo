<?php
namespace modules\admin;


use main\Db;

/**
 *  Класс Singleton для работы с правами доступа.
 * Class Access
 * @package modules\admin
 */
class Access {
    private static $instance;
    private $userRights = 0;
    private $modulesRights = array();

    private function __construct() {
        $this->setUserRights();
        $this->setModulesRights();
    }

    /**
     * Получает права текущего пользователя в двоичном формате.
     */
    private function setUserRights() {
        // TODO make a static variable.
        $id = (int)$_SESSION['mod_users_id'] ? $_SESSION['mod_users_id'] : 0;
        $user_rights = current(Db::queryExec("SELECT ref_user_groups.access_rights FROM ref_user_groups
                                              LEFT JOIN mod_users ON ref_user_groups.id = mod_users.group_id
                                              WHERE mod_users.id = :id", array(':id' => $id)));
        $this->userRights = base_convert($user_rights['access_rights'], 10, 2);
    }

    /**
     * Разбивает на массив строку с правами. Каждый элемент массива - двоичное число из трех цифр.
     */
    private function setModulesRights() {
        $this->modulesRights = explode( ',', wordwrap($this->userRights, 3, ',', true) );
        $modules = Db::queryExec("SELECT name FROM modules");
        $modules_amount = count($modules);
        if( $modules_amount !== count($this->modulesRights) ) {
            throw new \Exception('Modules quantity not equal to rights restrictions.');
        }

        $rights = array();
        for($key = 0, $l = $modules_amount; $key < $l; ++$key) {
            $rights[$modules[$key]['name']] = $this->modulesRights[$key];
        }
        $this->modulesRights = $rights;
    }

    /**
     * @return array
     */
    public static function getInstance()
    {
        if( empty(self::$instance) ) {
            self::$instance = new Access();
        }
        return self::$instance;
    }

    /**
     * @return array
     */
    public function getModulesRights()
    {
        return $this->modulesRights;
    }
}