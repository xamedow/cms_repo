<?php
/**
 * Date: 16.07.14
 * Time: 21:49
 */

namespace modules\admin;


use main\Db;
use main\MailHandler;
use main\Router;
use main\FileHandler as fh;
use main\SessionHandler;
use modules\UserHQ as uhq;


class AjaxHandler
{
    private $action = '';
    private $values = '';
    private $keys = '';
    private $table = '';
    private $rank = 1;
    private $writer;

    private $insert_arr = array();
    private $skipedItems = array(
        'action',
        'captcha',
        'table',
        'submit',
        'pass_change',
        'id'
    );
//TODO refactor
    public function __construct($dimensions = array(), $table = false)
    {
        if (!empty($_POST) && isset($_POST['action'])) {
            $this->action = $_POST['action'];
            $this->dimensions = $dimensions;
            $this->table = $table ? $table : 'mod_' . Router::getPageName();
            $this->setRank();
            $this->initKeysVals();
            $this->getMethod();
        }
    }

    private function setRank()
    {
        $rank = current(Db::queryExec("SELECT MAX(rank) as max FROM $this->table"));
        $this->rank = $rank['max'] + 1;
    }

    private function setWriter($dimensions)
    {
        $this->writer = new ImageWriter($dimensions);
    }

    private function initKeysVals()
    {
        if (!empty($this->writer) && $this->writer->allGood) {
            $this->setKeysVals($this->writer->getFieldName(), $this->writer->getFileName());
        }

        foreach ($_POST as $key => $val) {
            if (!in_array($key, $this->skipedItems)) {
                $val = ($key === 'pass' && !empty($val) && !empty($_POST['pass_change']))
                    ? substr(sha1(trim($val)), 4, -4) : $val;
                $val = ($key === 'rank' && empty($val)) ? $this->rank : $val;
                $val = $val === '' ? null : $val;
                $this->setKeysVals($key, $val);
            }
        }
        $this->keys = rtrim($this->keys, " \n\t,");
        $this->values = rtrim($this->values, " \n\t,");
    }

    private function setKeysVals($key, $val)
    {
        $this->insert_arr[':' . $key] = $val;
        $this->keys .= "$key,";
        $this->values .= ":$key,";
    }

    private function getMethod()
    {
        $method = $this->action . 'Data';
        if (method_exists($this, $method)) {
            $this->$method();
        } else {
            session_destroy();
        }
        die();
    }

    /**
     * Вставка нового элемента.
     * @throws \Exception
     */
    private function newData() {
        Db::queryExec("INSERT INTO {$this->table} ({$this->keys}) VALUES ({$this->values})", $this->insert_arr, false);
    }

    /**
     * Регистрация нового пользователя.
     * @throws \Exception
     */
    private function newRegData() {
        if ( !empty($_SESSION['captcha']) && !empty($_POST['captcha']) && $_SESSION['captcha'] === $_POST['captcha']) {
            try {
                // TODO make a distinct class for that.
                $pass = substr( md5(rand(0, 500)), 6, 6);
                $mailer = new MailHandler('Регистрационные данные от сайта ' . HHOST, $_POST['login'], 'info@' . HHOST, "логин для доступа к сайту - {$_POST['login']}, пароль - $pass");
                $mailer->send();
                $hash_pass = substr(sha1($pass), 4, -4);
                $this->keys .= ',pass';
                $this->values .= ',:pass';
                $this->insert_arr[':pass'] = $hash_pass;
                Db::queryExec("INSERT INTO {$this->table} ({$this->keys}) VALUES ({$this->values})", $this->insert_arr, false);
                $user = current(Db::queryExec("SELECT MAX(id) as max FROM {$this->table}"));

                $_SESSION['ref_clients_id'] = $user['max'];
                echo "Благодарим за регистрацию, Ваш пароль выслан на адрес: {$_POST['login']}.";
            } catch (\PDOException $e) {
                var_dump($e);
                echo 'К сожалению, имя пользователя уже занято.';
            }
        } else {
            echo 'Проверочный код неверен.';
        }
    }

    /**
     * Сохранение информации о текущей странице.
     */
    private function paginationData() {
        if(isset($_POST['table']) && isset($_POST['page'])) {
            $_SESSION[$_POST['table'] . '_page'] = $_POST['page'];
        }
    }
    private function getorderData() {

            $order_products = Db::queryExec("SELECT mod_catalog.id, mod_catalog.name, measure_in, price, quantity FROM mod_catalog
                                              LEFT JOIN comp_orders_catalog ON mod_catalog.id = catalog_id
                                              LEFT JOIN mod_orders ON mod_orders.id = order_id
                                              WHERE mod_orders.id = :id", array(':id' => $_POST['id']));
            echo json_encode($order_products);
            die();
    }
    /**
     * Обновление информации для элемента в админке.
     * @throws \Exception
     */
    private function saveData() {
        $keyval = explode(',', $this->keys);
        foreach($keyval as $i=>$key) {
            $keyval[$i] = "$key=:$key";
        }
        $keyval = implode(',', $keyval);
        Db::queryExec("UPDATE {$this->table} SET $keyval WHERE rank={$_POST['rank']}", $this->insert_arr, false);
    }
//TODO refactor

    /**
     * Удаление картинки.
     * @throws \Exception
     */
    private function deleteImgData() {
        Db::queryExec("UPDATE {$this->table} SET {$_POST['name']}='' WHERE rank={$_POST['rank']}", array(), false);
    }

    /**
     * Установка активности.
     * @throws \Exception
     */
    private function saveActData() {
        $act_elems = empty($_POST['act'])
            ? ''
            : implode(',', array_keys($_POST['act']));
        $all_elems = empty($_POST['elems'])
            ? ''
            : implode(',', array_keys($_POST['elems']));
        if (!empty($act_elems)) {
            Db::queryExec("UPDATE {$this->table} SET act=0 WHERE id IN($all_elems)", array(), false);
            Db::queryExec("UPDATE {$this->table} SET act=1 WHERE id IN($act_elems)", array(), false);
        }
    }

    /**
     * Удаление элемента из админки.
     * @throws \Exception
     */
    private function deleteItemsData() {
        if (!empty($_POST['del'])) {
            $delete_elems =  implode(',', array_keys($_POST['del']));
            $rank = current( Db::queryExec("SELECT rank FROM {$this->table} WHERE id = $delete_elems"));
            Db::queryExec("DELETE FROM {$this->table} WHERE id IN($delete_elems)", array(), false);
            Db::queryExec("UPDATE {$this->table} SET rank=rank-1 WHERE rank>{$rank['rank']}", array(), false);
        }
    }

    /**
     * ОБвление информации о рангах.
     * @throws \Exception
     */
    private function rankUpdateData() {
        if($_POST['direction'] !== 'equal') {
            $sign = $_POST['direction'] === 'up' ? '+' : '-';
            $new_eq = $_POST['direction'] === 'up' ? '>' : '<';
            $old_eq = $_POST['direction'] === 'up' ? '<' : '>';

            # Increment rank to substituted element.
            Db::queryExec(
                "UPDATE {$this->table} SET rank = rank $sign 1
                           WHERE rank $new_eq= {$_POST['index']} AND rank $old_eq= {$_POST['oldIndex']} AND id != {$_POST['id']}",
                array(),
                false
            );
            # Set new rank to dragged element.
            Db::queryExec("UPDATE {$this->table} SET rank={$_POST['index']} WHERE id={$_POST['id']}", array(), false);
        }
    }
}