<?php
namespace modules\admin;


use main\FileHandler;
use main\Router as r;

class ImageWriter {
    private $fieldName;
    private $fileName;
    private $dimensions;
    private $crop;
    private $path;
    private $ext;
    public $allGood = false;


    public function __construct(array $dimensions, $crop = false) {
        $this->dimensions = $dimensions;
        $this->crop = $crop;
        $this->fileName = rand();
        $this->path = DROOT . "/upload/" . r::getPageName();
        FileHandler::resolvePath($this->path);

        if($this->setFieldName()) {
            $this->saveImages();
            $this->allGood = true;
        }
    }

    private function saveImages() {
        // Определяем является ли массив многомерным.
        if(isset($this->dimensions[0])) {
            $this->saveImage($this->dimensions);
        } else {
            foreach($this->dimensions as $pref=>$dimens) {
                $this->saveImage($dimens, $pref);
            }
        }
    }
    private function saveImage($dimensions, $pref = '') {
        $path = $this->path . '/' . $pref . $this->fileName;
        $image = new \ImageHandler($_FILES[$this->fieldName]['tmp_name'], $path, $dimensions, $this->crop);
        $this->ext = $image->getExt();
    }

    private function setFieldName() {
        if( empty($_FILES)) {
            return false;
        }
        $this->fieldName = current(array_keys($_FILES));
        if($_FILES[$this->fieldName]['error']) {
            return false;
        }
        return true;
    }

    public function getFileName() {
        return $this->fileName . '.' .$this->ext;
    }

    public function getFieldName() {
        return $this->fieldName;
    }
}
