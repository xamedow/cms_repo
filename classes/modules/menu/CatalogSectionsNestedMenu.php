<?php

namespace modules\menu;
use main\Db as db;

class CatalogSectionsNestedMenu implements MenuInterface {
    public static function getElements() {
        return db::queryExec("SELECT id, parent, name, name as page_name, 'catalog' as module_name FROM mod_catalog_sections") ;
    }
} 