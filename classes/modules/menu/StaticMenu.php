<?php

namespace modules\menu;
use main\Db as db;

class StaticMenu implements MenuInterface {
    public static function getElements() {
        return db::queryExec("SELECT id, name, name as page_name, 'static' as module_name
                              FROM mod_static
                              WHERE in_menu='1' AND act=1 ORDER BY rank",
                            array() ) ;
    }
} 