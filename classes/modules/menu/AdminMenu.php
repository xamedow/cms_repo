<?php

namespace modules\menu;

use main\Db as db;
use modules\admin\Access;

/**
 *  Класс для работы с меню панели управления сайтом.
 * Class AdminMenu
 * @package modules\menu
 */
class AdminMenu implements MenuInterface
{

    /**
     *  Возвращает элементы меню системы управления.
     * @return array
     * @throws \Exception
     */
    public static function getElements()
    {
        $access = Access::getInstance();
        $modules_rights = $access->getModulesRights();
        $elems = array();
        $act_modules = db::queryExec('SELECT id, name, title, parent FROM modules WHERE act = 1 ORDER BY parent, rank');

        foreach ($act_modules as $key => $module) {
            $module_rights = $modules_rights[$module['name']];
            if ($module_rights & 100) {
                $elems[$key]['id'] = $module['id'];
                $elems[$key]['parent'] = $module['parent'];
                $elems[$key]['name'] = $module['title'];
                $elems[$key]['page_name'] = $module['name'];
                $elems[$key]['module_name'] = 'admin';
            }
        }

        return $elems;
    }
}