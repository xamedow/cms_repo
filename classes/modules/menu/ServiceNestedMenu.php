<?php

namespace modules\menu;
use main\Db as db;

class ServiceNestedMenu implements MenuInterface {
    public static function getElements() {
        return db::queryExec("SELECT id, parent, meta_title as name, name as page_name, 'service' as module_name FROM mod_service") ;
    }
} 