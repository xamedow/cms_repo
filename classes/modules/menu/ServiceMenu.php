<?php

namespace modules\menu;
use main\Db as db;

class ServiceMenu implements MenuInterface {
    public static function getElements() {
        return db::queryExec("SELECT mod_menu.id, mod_menu.name, page_name, modules.name as module_name
                              FROM mod_menu
                              LEFT JOIN modules ON module_id = modules.id WHERE menu_mode = 'service' ORDER BY mod_menu.rank") ;
    }
} 