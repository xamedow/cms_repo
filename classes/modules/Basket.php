<?php
/**
 * Created by PhpStorm.
 * User: itsme
 * Date: 08.10.2014
 * Time: 8:01
 */

namespace modules;


use main\SessionHandler;

class Basket extends Module {
    private $data;
    private $count = 1;
    private $actions = array(
        'addToBasket', 'deleteFromBasket', 'order'
    );

    public function getView() {
        $this->listen();
//        var_dump($_SESSION);
        if(empty($_SESSION['basket'])) {
            return $this->options;
        } else {
            return $this->getBasket();
        }
    }

    private function getBasket() {

        $products = $this->getProducts();
        $sum = $this->getSum();
        return <<<HTML
        <div class="true_basket">
            В вашей корзине $products<br>на сумму <span class="sum">$sum рублей</span>
            <a href="#">Оформить заказ</a>
        </div>
HTML;

    }

    private function getSum() {
        $out = null;
        if(is_array($_SESSION['basket'])) {
            foreach ( $_SESSION['basket'] as $product  ) {
                $out = (float)$product['price'] * (int)$product['quantity'];
            }

        }
        return $out;
    }

    private function listen() {
        if(!empty($_POST['action']) && in_array($_POST['action'], $this->actions)) {
            $this->data = $_POST;
            call_user_func(array($this, $_POST['action']));
        }
    }

    private function addToBasket() {
        $id = (int)$_POST['id'];
        $_SESSION['basket'][$id]['price'] = (float)$_POST['price'];
        $_SESSION['basket'][$id]['quantity'] = empty($_POST['quantity']) ? 1: (int)$_POST['quantity'];
//        die();
    }

    private function getProducts() {
        $this->count = count($_SESSION['basket']);
        foreach ($_SESSION['basket'] as $product) {
            $this->count = $product['quantity'] > 1 ? $this->count + $product['quantity'] : $this->count;
        }

        return $this->count . ' товар' . $this->getEnding($this->count);
    }

    private function getEnding($products) {
        $lastNum = substr((string)$products, -1);

        if($lastNum > '4' || $products == 11 || $lastNum == 0) {
            return 'ов';
        } else if($lastNum === '1') {
            return '';
        } else {
            return 'а';
        }
    }
} 