<?php
/**
 * Date: 09.05.14
 * Time: 21:19
 */

namespace modules;
use main\Auth;
use main\Db;
use main\Filter, main\FileHandler as fh;

/**
 * Модуль обработки форм заявок на обратный звонок.
 * Class Request
 * @package modules
 */
class UserHQ extends Module {

    public function getView() {
        $auth = Auth::getInstance('ref_clients');
        if(!$auth->getAuth()) {
            return $this->options;
        } else {
            $id = (int)$_SESSION['ref_clients_id'];
            $user = current(Db::queryExec("SELECT name FROM ref_clients WHERE id=:id", array(':id' => $id)));
            return <<<HTML
        <form class="hq">
            <p>Здравствуйте,<br>{$user['name']}</p>
            <a href="/lk/static.html">Личный кабинет</a>
            <a href="#" class="logout">Выйти</a>
        </form>
HTML;

        }
    }
}