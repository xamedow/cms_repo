<?php
/**
 * Date: 21.07.14
 * Time: 1:26
 */

namespace modules\preview;


use main\Db;
use main\Misc;

class ServicePreview extends \modules\Preview {

    protected function setData() {
        $this->data = Db::queryExec("SELECT * FROM $this->table WHERE front_page=1 AND trash=0");
    }

    private function getImg($path) {
        return "/upload/$this->options/sm$path";
    }

    private function getHref($href) {
        return "/$this->options/" . strtolower( Misc::getTranslit($href) ) . '.html';
    }

    public function getView()
    {
        $out = null;

        if (!empty($this->data)) {
            foreach ($this->data as $row) {
                $childRow = $this->getChildRow($row['id']);
                if (!empty($childRow)) {
                    $text = "&laquo;{$childRow['preview_text']}&raquo;";
                    $request = '<a href="" class="request"><div>подать заявку</div></a>';
                    $href = $this->getHref($childRow['name']);
                    $date = $this->getDateBlock($childRow['date_from'], $childRow['date_due'] );
                    $links = '';
                } else {
                    $text = $row['preview_text'];
                    $request = '';
                    $href = $this->getHref($row['name']);
                    $date = '';
                    $links = $this->getChildrenLinks();
                }
                $out .= <<<HTML
        <li>
			<div class="title">{$row['heading']}</div>
			<div class="blackout"></div>
            <img src="{$this->getImg($row['img'])}" alt="{$row['heading']}">
			<div class="cat_border">
			    $date
			    $links
				<div class="specify">
					$text
					$request
					<a href="$href" class="full">Подробнее</a>
				</div>
			</div>
		</li>
HTML;
            }
        }

        return $out;
    }

    /**
     * Возвращает подстраницу для замещения.
     *
     * @param $id
     *
     * @return bool
     */
    private function getChildRow($id)
    {
        $this->setChildren($id);
        if (!empty($this->children) && is_array($this->children)) {
            foreach ($this->children as $row) {
                if (!empty($row['front_page'])) {
                    return $row;
                }
            }
        }

        return false;
    }

    /**
     * Устанавливает свойству children массив детей указанного родителя.
     *
     * @param $id - родительский id
     */
    private function setChildren($id)
    {
        $this->children = Db::queryExec("SELECT * FROM $this->table WHERE parent=:parent AND act=1 ORDER BY rank", array(':parent' => $id));
    }

    /**
     * Возвращает url для изображения из папки upload.
     *
     * @param $path
     *
     * @return string
     */
    private function getImg($path)
    {
        return "/upload/$this->options/sm$path";
    }

    /**
     * Возвращает сформированный блок дат с датой от/до и мясецем/месяцами.
     *
     * @param string $from - строковое представление даты от
     * @param string $due - строковое представление даты до
     *
     * @return null|string
     */
    private function getDateBlock($from = '', $due = '')
    {
        $out = null;
        if (!empty($from)) {
            $from = $this->dateConstruct($from);
            $due = $this->dateConstruct($due, '-');
            $due['month'] = $due['month'] === $from['month'] ? '' : $due['month'];
            $out = "<div class='date'>{$from['day']}{$due['day']}<br>{$from['month']}<br>{$due['month']}</div>";
        }

        return $out;
    }

    /**
     *  Возвращает массив с ключами day(день с ведущими нулями) и month(месяц в русской локализации) сформированный
     * из аргумента date
     *
     * @param string $date
     * @param string $pref - разделитель для значения дня
     *
     * @return array
     */
    private function dateConstruct($date, $pref = '')
    {
        $out = array('day' => '', 'month' => '');
        if (!empty($date)) {
            $date_obj = new \DateTime($date);
            $out['day'] = $pref . strftime('%d', $date_obj->getTimestamp());
            $out['month'] = $this->transArr[strftime('%B', $date_obj->getTimestamp())];
        }

        return $out;
    }

    /**
     * Гененрирует html код ссылок для дочерних элементов, но не более двух.
     * @return null|string
     */
    private function getChildrenLinks()
    {
        $out = null;
        if (!empty($this->children)) {
            $children = count($this->children) > 2 ? array_slice($this->children, 0, 2) : $this->children;

            foreach ($children as $child) {
                $out .= "<a href='{$this->getHref($child['name'])}'>{$child['name']}</a>";
            }
        }

        return $out;
    }
    public function getView() {
        $out = null;

        if(!empty($this->data)) {
            foreach($this->data as $row) {


                $out .= <<<HTML
        <li>
			<div class="title">{$row['heading']}</div>
			<img src="{$this->getImg($row['img'])}" alt="{$row['heading']}">
			<div class="cat_border">
				<div class="specify">
					{$row['preview_text']}
					<a href="{$this->getHref($row['name'])}" class="full">Подробнее</a>
				</div>
			</div>
		</li>
HTML;
            }
        }
    }
} 