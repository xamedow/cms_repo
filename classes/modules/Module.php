<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 22.05.14
 * Time: 21:03
 */

namespace modules;


abstract class Module {
    protected $options;

    public function __construct($options) {
        $this->options = $options;
    }
    public function getView() {
        return $this->options;
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

} 