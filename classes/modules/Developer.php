<?php
/**
 * Date: 15.07.14
 * Time: 2:16
 */

namespace modules;


use main\Router;

class Developer extends Module {
    public function getView() {
        if(Router::getPageName() === 'glavnaya' && Router::getMainModule() === 'static') {
            return $this->options;
        }

        return null;
    }
} 