<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 09.05.14
 * Time: 21:19
 */

namespace modules;


use main\Db;
use main\Misc;

/**
 * Модуль для вставки контактной информации(e-mail, телефоны, адреса)
 * Class Contact
 * @package modules
 */
class Contact extends Module
{
    private static $contacts;
    private $currentCs = array();
    private $type;
    private $num = 0;

    /**
     *  Использует конструктор родителя, получает тип контакта и порядковый номер.
     * Если экземпляр класса был создан впервые, выбирает из БД все записи из таблицы mod_contacts.
     * Фильтрует записи по типу.
     * @param $options
     */
    public function __construct($options)
    {
        parent::__construct($options);

        if (strpos($options, '&')) {
            list($this->type, $this->num) = explode('&', $options);
        } else {
            $this->type = $options;
        }
        if (empty(self::$contacts)) {
            self::setContacts();
        }
        $this->setCurrentCs();
    }

    /**
     * Выбирает из БД все записи из таблицы mod_contacts.
     */
    private static function setContacts()
    {
        self::$contacts = Db::queryExec("SELECT name, type FROM mod_contacts WHERE trash=0");
    }

    /**
     * Фильтрует записи по типу.
     */
    private function setCurrentCs()
    {
        foreach (self::$contacts as $contact) {
            if ($contact['type'] === $this->type) {
                $this->currentCs[] = $contact['name'];
            }
        }
    }

    /**
     *  Возвращает контакт текущего типа и с указанным порядковым номером, если $num содержит цифры и элементы списка в
     * обратном случае.
     * @return mixed
     * @throws
     */
    public function getView()
    {
        if (!empty($this->currentCs)) {
            if (is_numeric($this->num)) {
                return $this->currentCs[$this->num];
            } else {
                $out = '';
                foreach ($this->currentCs as $contact) {
                    $out .= "<li>$contact</li>";
                }

                return $out;
            }
        } else {
            throw new \Exception("Wrong index: {$this->num} given for the contact.");
        }
    }
} 