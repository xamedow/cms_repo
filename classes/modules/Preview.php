<?php
namespace modules;


use main\Db;
use main\Misc;

/**
 * Класс для генерации различных наборов превью, услуги, каталог, новости и тд.
 * Class Preview
 * @package modules
 */
class Preview implements ModuleInterface
{
    protected $data;
    protected $table;
    protected $options;
    protected $children;
    private $transArr = array(
        ''          => '',
        'December'  => 'Декабря',
        'January'   => 'Января',
        'February'  => 'Февраля',
        'March'     => 'Марта',
        'April'     => 'Апреля',
        'May'       => 'Мая',
        'June'      => 'Июня',
        'July'      => 'Июля',
        'August'    => 'Августа',
        'September' => 'Сентября',
        'October'   => 'Октября',
        'November'  => 'Ноября',
    );

    public function __construct($options)
    {
        $this->options = trim($options);
    }

    /**
     * Устанавливает свойсвто table из предоставленной опции.
     */
    protected function setTable()
    {
        $this->table = 'mod_' . $this->options;
    }

    protected function setData()
    {
        $this->data = Db::queryExec("SELECT * FROM $this->table WHERE act = 1");
    }



    /**
     * Генерирует url для аттрибута href из кириллического названия элемента и переданной в конструктор опции.
     *
     * @param $name - название элемента в кириллице
     * @param $parent_name - название родительского элемента в кириллице
     *
     * @return string - сформированный url
     */
    private function getHref($name, $parent_name = null)
    {
        $parent_name = $parent_name ? strtolower(Misc::getTranslit($parent_name)) . '/' : null;
        return "/catalog/" . $parent_name . strtolower(Misc::getTranslit($name)) . '.html';
    }

    private function getImg($src) {
        $src = "/upload/{$this->options}/sm$src";
        return file_exists(DROOT . $src) ? $src : '/images/empty.jpg';
    }

    private function init() {
        $this->setTable();
        $this->setData();
    }

    private function getChildrenLinks ($parent_id, $parent_name) {
        $out = null;
        if(is_array($this->data)) {
            foreach ($this->data as $key => $row) {
                if($row['parent'] === $parent_id) {
                    $href = $this->getHref($row['name'], $parent_name);
                    $out .= "<li><a href=\"$href\">{$row['name']}</a></li>";
                }
            }
            $out = empty($out) ? null : <<<HTML
                <div class="sub_catalog">
                    <div class="cat_name"></div>
                    <ul>
                        $out
                    </ul>
                </div>
HTML;

        }
        return $out;
    }

    public function getView() {
        $this->init();
        $out = null;
        foreach ($this->data as $key => $row) {
            if ($row['front_page'] === '1' && $row['parent'] === '0') {
                $img = $this->getImg($row['img']);
                $href = $this->getHref($row['name']);
                $children = $this->getChildrenLinks($row['id'], $row['name']);

                $out .= <<<HTML
        <li>
            <div class="catalog_image">
                <img src="$img" alt="{$row['name']}">
            </div>
            <a href="$href">{$row['name']}</a>
            $children
        </li>
HTML;
            }
        }
        return $out;
    }
} 