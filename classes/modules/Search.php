<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 09.05.14
 * Time: 21:19
 */

namespace modules;


use main\Db;
use main\FileHandler;
use main\Filter;
use main\Misc;

class Search extends Module implements ModuleInterface {
    /**
     *  Объект настроек.
     *  searchIn - Модуль в котором производится поиск.
     *  fields - Поля по которым производится поиск.
     * @var array
     */
    private $config = array(
        'searchIn' => 'mod_catalog',
        'fields' => array(
            'name'
        )
    );

    /**
     * Поисковый запрос.
     * @var null
     */
    private $searchQuery = null;
    /**
     * Поисковая выдача.
     * @var array
     */
    private $searchData = array();

    /**
     * Поля поиска и данные запроса для SQL.
     * @var null
     */
    private $queryFields = null;

    /**
     * Проверяет соответствие таблицы и полей, указанных в настройках
     * @return bool
     */
    private function checkFields() {
        return Db::fieldsInTable($this->config['fields'], $this->config['searchIn']);
    }

    private function setSearchQuery() {
        if( isset($_POST['search_submit']) && isset($_POST['search_text']) ) {
            $filter = new Filter($_POST['search_text'], 'string');
            $this->searchQuery = $filter->apply();
        }
    }

    /**
     *  Конструктор sql подзапроса.
     * @return null|string
     */
    private function setQueryFields() {
        $out = null;
        if ( is_array($this->config['fields']) ) {
            foreach ($this->config['fields'] as $field) {
                $out .= "OR $field LIKE '%{$this->searchQuery}%'";
            }
            $out = ' WHERE ' . trim($out, " OR\t");
        }
        $this->queryFields = $out;
    }

    /**
     * Поиск по БД.
     * @throws \Exception
     */
    private function setSearchData() {
        if($this->checkFields()) {
            $this->setSearchQuery();
            $this->setQueryFields();
            $query = "SELECT id FROM {$this->config['searchIn']}{$this->queryFields}";
            $this->searchData = Db::queryExec($query);
        }
    }

    /**
     *
     * @return mixed
     */
    public function getView() {
        if(isset($_POST['search_submit'])) {
            $this->setSearchData();
            FileHandler::writeTo('logs/temp', var_export($this->searchData, true));
        }
        return $this->options;
    }
} 