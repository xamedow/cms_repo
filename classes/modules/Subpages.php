<?php
namespace modules;


use main\Db;
use main\Misc;
use main\Router;

class Subpages implements ModuleInterface {
    private $options;
    private $data;
    private $table;

    public function __construct($options) {
        $this->options = trim($options);
        $this->table = 'mod_' . $this->options;
        $this->setData();
    }

    private function setData() {
        $page = Router::getPageName();
        $rupage = Misc::getTranslit($page, 'ru');
        if (Db::fieldsInTable(array('parent', 'rank'), $this->table)) {
            $this->data = Db::queryExec("SELECT * FROM {$this->table}
                                         WHERE parent=(SELECT id FROM {$this->table} WHERE name=:name)",
                array(':name'=>$rupage));
        }
    }

    private function getHref($name) {
        $name = lcfirst(Misc::getTranslit($name));
        return 'http://' . HHOST . "/$this->options/$name.html";
    }

    public function getView() {
        $out = null;
        if( !empty($this->data) && is_array($this->data) ) {
            foreach($this->data as $row) {
                $href = $this->getHref($row['name']);
                $img_src = empty($row['img']) ? '/images/empty.jpg' : "/upload/{$this->options}/sm{$row['img']}" ;
                $name = wordwrap($row['name'], 50, '<br>');
                $out .= <<<HTML
                <li>
                    <h4>$name</h4>
                    <a href=$href><img src='$img_src' alt=''><span>подробнее</span></a>
                </li>
HTML;
            }
            $out = "<ul class='sub_pages'>$out</ul>";
        }
        return $out;
    }
} 