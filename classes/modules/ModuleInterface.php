<?php

namespace modules;


interface ModuleInterface {
    public function getView();
} 