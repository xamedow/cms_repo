<?php
/**
 * Date: 23.07.14
 * Time: 10:22
 */

namespace modules;


use main\Db;
use main\Router;

class Meta implements ModuleInterface {
    private $options;
    private $data;

    public function __construct($options) {
        $this->options = trim($options);
        $this->setData();
    }

    private function setData() {
        if(Db::fieldsInTable(array($this->options), 'mod_meta')) {
            $this->data = current( Db::queryExec("SELECT {$this->options} FROM mod_meta LIMIT 1") );
        }
    }

    public function getView() {
        return (!empty($this->data) && array_key_exists($this->options, $this->data)) ? $this->data[$this->options] : '';
    }
} 