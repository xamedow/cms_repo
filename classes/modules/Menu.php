<?php

namespace modules;
use main\ListHandler, main\Db as db, main\Router as r;

/**
 * Class Menu
 * @package modules
 */
class Menu extends ListHandler implements ModuleInterface {
    /**
     * @var string
     */
    private $options;
    private $type = '';

    /**
     * @param string $options
     */
    public function __construct($options) {
        $options = trim($options);
        if (strpos($options, '&') === false) {
            $this->options = $options;
        } else {
            list($this->options, $this->type) = explode('&', $options);
        }
        $this->setDataArray();
        $mod_name = isset( $this->data_arr[0]['module_name']) ? $this->data_arr[0]['module_name'] : $this->options;
        parent::__construct(r::getPageName(), $mod_name);
    }

    /**
     * Выбирает класс-хелпер и отдает полученные оттуда данные в родительский класс.
     */
    private function setDataArray() {
        $module = '\modules\menu\\'.ucfirst($this->options).ucfirst($this->type).'Menu';
        if (method_exists($module,'getElements')) {
            parent::setDataArr($module::getElements());
        } else {
            throw new \Exception("Wrong class name '$module' given in menu handling.");
        }
    }

    /**
     * Вовзвращает сборщику контент, для подстановки в представление.
     * @return bool|string
     */
    // TODO refactor
    public function getView() {
        $is_admin = $this->options === 'admin';
        return parent::getLi(0, $is_admin);
    }
}