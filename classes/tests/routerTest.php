<?php
/**
 * Created by PhpStorm.
 * User: itsme
 * Date: 29.09.2014
 * Time: 10:07
 */

namespace tests;

require_once '../main/Router.php';
require_once '../main/FileHandler.php';
require_once '../main/Db.php';
require_once '../main/Misc.php';
use main\Db;
use main\Router;
use main\Misc;
use main\FileHandler;

class RouterTest extends \PHPUnit_Framework_TestCase {
    private $url = 'http://astera/catalog/parent/child.html';

    public function setUp() {
        # Определяем константы.
        define('RURI', 'http://astera64.ru/static/page.html');
        define('DROOT', 'c:\\webhosts\\astera');
        define('HHOST', 'http://astera64.ru/static/page.html');

            $db = Misc::get_param(FileHandler::readToArray('../../conf/db'));

# Соединяемся с бд и выставляем кодировку.
            Db::init($db);
            Db::queryExec("SET NAMES utf8", array(), false);

            new Router($this->url);

    }

    public function testRoute() {
        $page_name = Router::getPageName();
        $page_sections = Router::getSectionName();

        $this->assertEquals("glavnaya", $page_name, 'Testing home page');
    }
} 