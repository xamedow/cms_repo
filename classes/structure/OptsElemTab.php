<?php
/**
 * Date: 18.07.14
 * Time: 11:07
 */

namespace structure;

use main\Db, main\FormHandler as fm;
use main\Router;

class OptsElemTab extends Tab
{

    protected function setData()
    {
        $order = Db::fieldsInTable(array('rank'), $this->table) ? ' ORDER BY rank' : '';
        $this->data = Db::queryExec("SELECT * FROM {$this->table} $order");
    }

    private function getInnerTabs()
    {
        $this->opts['id'] = "form_{$this->table}_1";
        $admin_form = fm::getInstance($this->opts, 'Admin', $this->data[0]);
        $out = <<<HTML
                <div id="{$this->table}_tab-1">
                    {$admin_form->getFormView()}
                </div>
HTML;

        return $out;
    }

    public function getTab()
    {
        return <<<HTML
        <div id="inner_tabs">
            {$this->getInnerTabs()}
        </div>
HTML;
    }
} 