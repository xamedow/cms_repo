<?php
namespace structure;


use main\Router;

abstract class Tab {
    protected $table;
    protected $data;
    protected $tab_index;
    protected $opts = array(
        'fields' => array("submit" => array('type' => 'submit', 'class' => 'save submit', 'value' => 'Сохранить')),
        'label' => true,
        'class' => 'admin_form'
    );

    abstract public function getTab();
    abstract protected function setData();

    public function __construct($table = '') {
        $this->table = $table;
        $this->setTable();
        $this->setData();
    }

    protected function setTable() {
        if (!$this->table) {
            $this->table = 'mod_' . Router::getPageName();
        }
    }

    protected function getTable() {
        return $this->table;
    }
} 