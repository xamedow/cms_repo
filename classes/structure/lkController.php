<?php
/**
 * Created by PhpStorm.
 * User: itsme
 * Date: 29.09.2014
 * Time: 13:12
 */

namespace structure;


use main\Db;
use main\FileHandler, main\Misc as mi;
use main\Misc;

class lkController extends Controller {
    private $orders;
    private $creds;

    public function init($id) {
        $this->setOrders($id);
        $this->setCreds($id);
    }

    /**
     * @return mixed
     */
    public function getCreds()
    {
        return $this->creds;
    }



    /**
     * @return mixed
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param $id
     * @throws \Exception
     */
    private function setOrders($id) {
        $orders_data = Db::queryExec("SELECT id, date FROM mod_orders WHERE client_id=:id ORDER BY date", array(':id' => $id));
        if(!empty($orders_data)) {
            $count = 1;
            foreach ($orders_data as $row) {

                $this->orders .= <<<HTML
                    <li>
                        <a href="#" data-id="{$row['id']}">Заказ #$count, от {$row['date']} </a>
                        <a class="fold_out" href="#">&#9650;</a>
                    </li>
HTML;
                ++$count;
            }
            $this->orders = "<ul class='order_list'>$this->orders</ul>";
        }
    }

    private function setCreds($id) {
        $data = current( Db::queryExec("SELECT * FROM ref_clients WHERE id = :id", array(':id' => $id)) );

        $this->creds = <<<HTML
        <form class="reg_save" action="">
                <ul>
                    <li><label for="user_name">ФИО или название организации</label>
                        <input value="{$data['name']}" required="required" id="user_name" name="user_name" type="text"/></li>
                    <li><label for="user_email">Электронная почта</label>
                        <input value="{$data['login']}" required="required" id="user_email" name="user_email" type="text"/></li>
                    <li><label for="user_phone">Контактный телефон</label>
                        <input value="{$data['phone']}" required="required" id="user_phone" name="user_phone" type="text"/></li>
                    <li><label for="user_address">Адрес доставки</label>
                        <input value="{$data['address']}" required="required" id="user_address" name="user_address" type="text"/></li>
                    <li><label for="user_pass">Пароль</label>
                        <input placeholder="Оставьте поле пустым, если не хотите изменить пароль." required="required" id="user_pass" name="user_pass" type="text"/></li>
                </ul>
                <input type="submit" value="Сохранить"/>
        </form>
HTML;

    }
} 