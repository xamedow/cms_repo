<?php
/**
 * Date: 15.07.14
 * Time: 15:35
 */

namespace structure;


use main\Db;
use main\FormHandler;

class NewElemTab extends Tab
{
    private $form;

    public function __construct($table = '')
    {
        parent::__construct($table);
        $this->opts['fields']['submit']['value'] = 'Создать';
        $this->opts['id'] = 'form_new';
        $this->setForm();
    }

    /**
     * Делает срез полей из установленной таблицы.
     * Устанвливает каждому полю из среза пустое значение и записывает его в свойство data.
     */
    protected function setData()
    {
        $fields = Db::getTableFields(parent::getTable());
        array_walk($fields, array('structure\NewElemTab', 'setField'));
    }

    /**
     * Конструирует форму.
     */
    private function setForm()
    {
        $this->form = FormHandler::getInstance($this->opts, 'Admin', $this->data);
    }

    /**
     *  Возвращает html код внешнего таба.
     * @return string
     */
    public function getTab()
    {
        return <<<HTML
        <div id="inner_tabs">
            {$this->form->getFormView()}
        </div>
HTML;

    }

    /**
     * Обнуляет поле формы.
     * @param $val
     */
    private function setField($val)
    {
        $this->data[$val['Field']] = '';
    }
} 