<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 26.05.14
 * Time: 23:00
 */

namespace structure;

use main\Db;
use modules\admin\Access, main\Router as r, main\FormHandler as fm;

class AdminModel extends Model {
    private $table = '';
    private $page_name = '';
    private $actions = array();
    private $modulesRights = array();
    private $tabsLi = '';
    private $tabsDiv = '';

    function __construct($name, $type, $page_name) {
        parent::__construct($name, $type, $page_name);
        $this->page_name = r::getPageName();
        $this->table = 'mod_' . $this->page_name;
        $this->setModulesRights();
        $this->setActions();
        $this->setTabs();
    }

    public function getModules() {
        $act_modules = db::queryExec('SELECT name, title FROM modules WHERE act = 1');
        $data = null;
        foreach ($act_modules as $module) {
            if (file_exists(DROOT . '/modules/' . $module['name'])) {
                $data .= "
                    <li>
                        <img src='/images/admin/{$module['name']}.png'>
                        <a class='button button-circle' href=\"/admin/{$module['name']}.html\">{$module['title']}</a>
                    </li>";
            } else {
                throw new Exception("Module - {$module['name']} was set active, but no files found. Probably module name misspell in db.");
            }
        }
    }

    private function setModulesRights() {
        $access = Access::getInstance();
        $this->modulesRights = $access->getModulesRights();
    }
    
    private function setActions() {
        if(array_key_exists($this->page_name, $this->modulesRights)) {
            $this->actions = Db::queryExec("SELECT ref_modules_actions.name, ref_modules_actions.type, ref_modules_actions.table
                                             FROM ref_modules_actions
                                             LEFT JOIN comp_modules_actions ON ref_modules_actions.id = action_id
                                             LEFT JOIN modules ON modules.id = module_id WHERE modules.name=:name
                                             ORDER BY ref_modules_actions.rank",
                array(':name'=>$this->page_name));
        }
    }
    private function getTabData($type, $table) {
        // TODO fix this.
        $name = ucfirst($type) . 'ElemTab';
        switch($type) {

            case 'new':
                $obj = new NewElemTab($table);
                break;
            case 'opts':
                $obj = new OptsElemTab($table);
                break;
            default:
                $obj = new ListElemTab($table);
        }
        return empty($obj) ? '' : $obj->getTab();
    }

    private function setTabs() {
        $out = null;

        if(!empty($this->actions)) {
            $module_rights = str_split( $this->modulesRights[$this->page_name] );
            for($i = 0; $i < count($this->actions); $i++) {
                if((int)$module_rights[$i]) {
                    $tab = $i + 1;

                    $this->tabsLi .= "<li><a href='#tab-$tab'>{$this->actions[$i]['name']}</a></li>";
                    $this->tabsDiv .= <<<HTML
                        <div id="tab-$tab" class="main_table">
                            {$this->getTabData($this->actions[$i]['type'], $this->actions[$i]['table'])}
                        </div>
HTML;
                }
            }
        }

        return $out;
    }

    /**
     * Возвращает разметку под блоки горизонтальных табов(список, настройки и т.д.).
     * @return null|string
     */
    public function getTabsDiv() {
        return $this->tabsDiv;
    }

    /**
     * Возвращает разметку под элементы списка для вертикальных табов(список, настройки и т.д.).
     * @return null|string
     */
    public function getTabsLi() {
        return $this->tabsLi;
    }
}