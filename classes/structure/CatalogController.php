<?php
/**
 * Created by PhpStorm.
 * User: itsme
 * Date: 29.09.2014
 * Time: 13:12
 */

namespace structure;


use main\FileHandler, main\Misc as mi;
use main\Misc;

class CatalogController extends Controller {
    private $data;
    private $productsList;

    public function processProductCard($data) {
        $this->data = $data;
        $this->processProductCardData();
        $view = $this->readView('secondary', 'catalog_product_card.php');
        return $this->setProductCard($view);
    }

    public function processProductCardData() {
        if($this->data ) {
            $this->data['img'] = FileHandler::getImgSrc($this->data['img']);
            $this->data['price'] = $this->makePrice($this->data['price']);
        }
    }

    public function processProductsList($data, $substitute, $view = null) {
        $this->data = $data;
        if(!$view) {
            $view = $this->readView('secondary', 'catalog_products_list.php');
            $this->setProductList();
            $data = $this->productsList;
        }
        return $this->replaceView($substitute, $data, false, $view);
    }

    private function setProductCard($view) {
        if (is_array($this->data)) {
            foreach ($this->data as $key => $val) {
                $view = $this->replaceView($key, $val, false, $view);
            }
            return $view;
        }
        return null;
    }

    private function setProductList() {
        $out = null;
        if(is_array($this->data)) {
            foreach ($this->data as $product) {
                $img = isset($product['img']) ? $product['img'] : null;
                $img_src = FileHandler::getImgSrc("/upload/catalog/$img");
                $href = $this->getHref($product['id']);
                $price = $this->makePrice($product['price']);
                $out .= <<<HTML
            <li>
                <a href="$href">
                    <span class="product_name">{$product['name']}</span>
                    <img src="$img_src" alt="{$product['name']}"/>
                    <span class="price">$price</span>
                    <span class="more">подробнее</span>
                </a>
                <a href="" class="one_click" data-id="{$product['id']}">Заказ в один клик</a>
            </li>
HTML;
            }
        } else {
            $out = $this->data;
        }
        $this->productsList = $out;
    }

    private function getHref($name) {
        $name = strtolower(mi::getTranslit($name, 'en'));
        return empty($name) ? null : substr(RURI, 0, -5) . "/$name.html";
    }

    private function makePrice($price) {
        return empty($price)
            ? 'По запросу.'
            : $price . ' <sub>руб.</sub>';
    }
} 