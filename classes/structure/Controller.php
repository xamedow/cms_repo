<?php
namespace structure;

use main\FileHandler as fh, main\Router as r;

/**
 * Class Controller
 * Методы для контроллеров всех модулей.
 * @package main
 */
class Controller extends ModuleHandler
{
    /**
     * Имя файла с представлением.
     * @var string
     */
    public  $view = 'admin.php';


    public function __construct($name, $type) {
        parent::__construct($name, $type);
        $this->setView($name.'.php');
    }
    /**
     * Аксессоры.
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }
    public function setView($view)
    {
        $this->view = $view;
    }

    /**
     * Считывает файл представления в строку и присвает строку свойству view текущего объекта.
     * @param $type - переопределение свойства type.
     * @param $view - переопределение свойства view.
     * @return string
     */
    public function readView($type = null, $path = null)
    {
        $is_main = false;
        if(!($type && $path)) {
            $type = $this->type;
            $path = $this->view;
            $is_main = true;
        }
        $out = fh::readToString(DROOT . "/views/$type/$path");
        if ($is_main) {
            $this->view = $out;
        } else {
            return $out;
        }
    }

    /**
     * Возвращает строку, состоящую из представления для текущей страницы + общие представления
     */
    public function readViews() {
        $this->view .= fh::readToString(DROOT . '/views/main/header.php');
        $this->view = fh::readToString(DROOT . '/views/main/' . $this->view);
        $this->view .= fh::readToString(DROOT . '/views/main/footer.php');
    }

    /**
     * Заменяет якорь в представлении значением.
     * @param $anchor string
     * @param $value string
     * @param $noregex bool
     * @param $view bool/string
     * @return string
     */
    public function replaceView($anchor, $value, $noregex = true, $view = null)
    {
        $is_main = !$view;
        $view = $view ? $view : $this->view;
        $out = $noregex
            ? str_ireplace($anchor, $value, $view)
            : preg_replace("/<!--{{(\b$anchor\b).*}}-->/i", $value, $view);
        if($is_main) {
            $this->setView($out);
        }
        return $out;
    }
}

