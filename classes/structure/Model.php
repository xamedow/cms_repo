<?php
namespace structure;

use main\FileHandler as fh, main\Misc as mi, main\Db as db, main\ErrorHandler as e;
use main\Router;

/**
 * ОБщий набор методов для всех моделей модулей.
 * Class Model
 * @package main
 */
class Model extends ModuleHandler
{
    private $page_name;
    private static $scripts = array();
    private static $styles = array();

    function __construct($name, $type, $page_name = null)
    {
        parent::__construct($name, $type);
        $this->page_name = $page_name ? $page_name : Router::getPageName();
        if($name === 'admin') {
            self::$scripts = empty(self::$scripts) ? self::getAssets('head', 'js', true) : self::$scripts;
            self::$styles = empty(self::$styles) ? self::getAssets('head', 'css', true) : self::$styles;
        } else {
            self::$scripts = empty(self::$scripts) ? self::getAssets('head') : self::$scripts;
            self::$styles = empty(self::$styles) ? self::getAssets('head', 'css') : self::$styles;
        }
    }

    protected function getTable()
    {
        return 'mod_' . $this->name;
    }

    /**
     * Возвращает транслитерированную из английского в русский строку с именем страницы.
     * @return string
     */
    protected function getRuPageName()
    {
        return mi::getTranslit($this->page_name, 'ru');
    }


    /**
     * Возвращает метаданные для текущего модуля и страницы.
     * @return mixed
     */
    function getMeta()
    {
        $meta = array();
//        $module = ($this->name === 'head') ? 'landing' : $this->name;
        $module = Router::getMainModule();
        if (db::fieldsInTable(array('meta_title', 'meta_keywords', 'meta_description'), 'mod_'.$module)) {
            $page = ucfirst($this->getRuPageName());
            $meta = current(db::queryExec(
                "SELECT meta_title as title, meta_keywords as keywords, meta_description as description FROM mod_$module WHERE LOWER(name)=:page",
                array(':page' => $page)
            ));
        }
        $main_meta = current( db::queryExec("SELECT * FROM mod_meta") );
        foreach($main_meta as $key=>$val) {
            if( empty($meta[$key]) ) {
                $meta[$key] = $main_meta[$key];
            }
        }

        return $meta;
    }

    /**
     * Выбирает метод обработки имени страницы.
     * @return mixed|null|string
     */
    private function choosePageName() {
        return preg_match('/[a-z]/i', $this->page_name) === 1 ? $this->getRuPageName() : $this->page_name;
    }

    /**
     * Возвращает заголовок и контент для основного модуля.
     * @param $byField - поле для
     * @return mixed
     */
    function getContent($byField = false)
    {
        $substring = '';
        $subst = array();
        if($this->name === 'static' || $this->name === 'catalog') {
            $byField  = $byField ? $byField : 'name';
            $substring = " AND LOWER($byField) = :$byField";
            $val = $this->choosePageName();
            $subst = array(":$byField" => $val);
        }

        return db::queryExec("SELECT * FROM {$this->getTable()} WHERE act=1$substring ORDER BY rank", $subst);
    }


    /**
     *  Возвращает ряд из таблицы modules_confmap, содержащий общее представление,
     * представление для конкретной страницы и имя страницы.
     * @return mixed
     */
    private function getViewsRow()
    {
        $table = $this->getTable();
        $query = "SELECT common_view, dedicated_view, name FROM modules_confmap
                    LEFT JOIN $table ON modules_confmap.page_id = $table.id
                    WHERE module = :module_name";

        try {
            if ($this->name && $this->name !== 'glavnaya') {
                return current(db::queryExec($query, array(
                    ':module_name' => $this->name
                )));
            }
            return false;
        } catch (\PDOException $e) {
            e::log('db_errors', $e, true);
        } catch (\Exception $e) {
            e::log('common_errors', $e, true);
        }
    }

    /**
     * Возвращает имя файла представления для текущей страницы.
     * @return mixed
     * @throws \Exception
     */
    function getView()
    {
        $views = $this->getViewsRow();
        if ($views) {
            if (mb_strtolower($views['name'], 'UTF-8') === $this->getRuPageName()) {
                $view =  $views['dedicated_view'];
            } else {
                $view = $views['common_view'];
            }
            return $view . '.php';
        } else {
            throw new \Exception('Views to module "' . $this->name . '" not found: ');
        }
    }

    private static function readAssetsFiles($type = 'js', $path)
    {
        $abs_path = fh::resolvePath( DROOT . $path);
        try {
            return fh::filesToArray($abs_path, $type);
        } catch (\Exception $e) {
            e::log('file_errors', $e, true);
            return null;
        }
    }

    /**
     * Возвращает относительный путь к папке элементов указанного типа, в зависимости от флага $admin.
     * @param $name - имя модуля
     * @param $type - тип элемента
     * @param $admin - флаг для элементов системы администрирования
     *
     * @return string
     */
    private static function getAssetsPath($name, $type, $admin) {
        return $admin ? "/modules/admin/assets/$type" : "/assets/$name/$type";
    }

    /**
     * Проверяет, является ли файл,
     * @param $asset - имя файла
     * @param $path - относительный путь к файлу
     *
     * @return string
     */
    private static function checkCDN($asset, $path) {
        if(strpos($asset, 'cdn') !== false) {
            return fh::readToString(DROOT . $path . '/' . $asset);
        } else {
            return 'http://' . HHOST . $path . '/' . $asset;
        }
    }

    private static function getAssets($name, $type = 'js', $admin = false) {
        $out = array();
        $path = self::getAssetsPath($name, $type, $admin);
        $assets_arr = self::readAssetsFiles($type, $path);
        if (!empty($assets_arr)) {
            foreach ($assets_arr as $asset) {
                if($type === 'js') {
                    $key = "\t<script src='" . self::checkCDN($asset, $path) . "'></script>";
                } else {
                    $key = "\t<link rel='stylesheet' href='" . self::checkCDN($asset, $path) . "'>";
                }
                $out[$key] = '';
            }
        }
        return $out;
    }


    /**
     * Возвращает отсортированную строку со скриптами из статического свойства $scripts.
     * @return string
     */
    public static function getScripts()
    {
        ksort(self::$scripts);
        return implode("\n", array_keys(self::$scripts));
    }

    /**
     * Возвращает отсортированную строку со стилями из статического свойства $styles.
     * @return string
     */
    public static function getStyles()
    {
        ksort(self::$styles);
        return implode("\n", array_keys(self::$styles));
    }

} 