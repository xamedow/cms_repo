<?php

namespace structure;

/**
 * Class ModuleHandler
 * Общий, абстрактный класс для классов Модели и Конструктора.
 * Свойства - имя модуля и тип.
 * A common class for both Model and Controller classes.
 * Takes the name of the module and its type, main or secondary.
 * @package main
 */
abstract class ModuleHandler {
    protected $name;
    protected $type;

    function __construct($name, $type)
    {
        $this->name = $name;
        $this->type = $type;
    }

    /**
     * Возвращает абсолютный либо относительный путь к файлам модуля.
     * @param bool $abs
     * @return string
     */
    protected function get_path($abs = false)
    {
        $abs = $abs ? $abs . '/' : '';
        return $abs."modules/$this->type/$this->name";
    }

    /**
     * Возвращает тип модуля.
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    abstract function getView();

} 