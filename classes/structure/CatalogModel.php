<?php
/**
 * Created by PhpStorm.
 * User: itsme
 * Date: 29.09.2014
 * Time: 9:39
 */

namespace structure;

use main\Db;
use main\FileHandler;
use main\Router as r;
use main\Misc as mi;

class CatalogModel extends Model {

    private $products;

    private function selectProducts() {
        if(r::getSectionName()) {
            $subst = array(':name' => $this->getRuPageName());
            return Db::queryExec(
                "SELECT mod_catalog.* FROM mod_catalog_sections
                  RIGHT JOIN mod_catalog ON mod_catalog.section_id = mod_catalog_sections.id
                  WHERE mod_catalog_sections.act=1 AND
						(mod_catalog_sections.parent = (SELECT mod_catalog_sections.id FROM mod_catalog_sections WHERE LOWER(mod_catalog_sections.name)=:name LIMIT 1 )
						OR LOWER(mod_catalog_sections.name)=:name)
                  ORDER BY mod_catalog_sections.rank"
                , $subst);
        }
    }
    private function setProducts($products) {
        $this->products = $products ? $products : null;
    }

    public function getSectionContent() {
        $substring = ' AND LOWER(name) = :name';
        $subst = array(':name' => $this->getRuPageName());
        return db::queryExec("SELECT * FROM mod_catalog_sections WHERE act=1$substring ORDER BY rank", $subst);
    }
    /**
     * @return mixed
     */
    public function getProducts()
    {
        $this->setProducts($this->selectProducts());
        return $this->products;
    }


} 