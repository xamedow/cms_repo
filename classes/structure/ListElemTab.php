<?php
namespace structure;
use main\Db, main\FormHandler as fm;
use main\Misc;
use main\Paginator;
use main\Router;

class ListElemTab extends Tab {
    private $paginator;
    protected function setData() {
        if (Db::fieldsInTable(array('name', 'rank'), $this->table)) {
            $parent = Db::fieldsInTable(array('parent'), $this->table) ? 'parent,' : '';
            $this->paginator = new Paginator($this->table, 15);
            $limit = $this->paginator->getLimit();
            $this->data = Db::queryExec("SELECT * FROM {$this->table} WHERE trash=0 ORDER BY {$parent}rank$limit");
        }
    }

    /**
     * Возвращает url элемента на внешнем сайте.
     * @param $page
     * @return null|string
     */
    private function getOuterHref($page) {
        $page = lcfirst($page);
        $module = substr($this->table, 4);
        if($module === 'landing') {
            return 'http://'. HHOST . "/#$page";
        }
        $path = DROOT . '/modules/' . $module;
        if(file_exists($path)) {
            return 'http://'. HHOST . "/$module/$page.html";
        }

        return null;
    }
    private function getParent($id) {
        return current(Db::queryExec("SELECT * FROM {$this->table} WHERE id=:id", array('id'=>$id)));
    }

    /**
     * Если у результата выборки все элементы имеют индекс родителя(parent), но результат не содержит родителя,
     * удаляем родительский индекс.
     */
    private function checkForParents() {
        $children = array();
        $parents = array();
        foreach($this->data as $row) {
            if (isset($row['parent'])) {
                if((int)$row['parent'] === 0) {
                    $parents[$row['id']] = true;
                } else {
                    $children[$row['parent']] = true;
                }
            }
        }
        $parents = array_keys($parents);
        foreach($children as $key => $val) {
            if(!in_array($key, $parents)) {
                $this->data[] = $this->getParent($key);
            }
        }
    }

    /**
     * Собирает закладки для внутреннего списка табов.
     * @param int $nest
     * @return null|string
     */
    // TODO refactor this!
    private function getInnerTabsLi($nest = 0) {
        $out = null;
        $class = $nest ? ' class="indented"' : '';
        if (!empty($this->data)) {
            $this->checkForParents();
            for($i = 1, $l = count($this->data); $i <= $l; $i++) {
                $row = $this->data[$i - 1];
                $parent = (empty($row['parent'])) ? '0' : $row['parent'];
                $id = ' id="innerLi_' . $row['id'] . '"';
                if ($nest == $parent) {
                    $page = Router::getPageName();
                    $checked = empty($row['act']) ? '' : ' checked="checked"' ;
                    $name = $row['name'];
                    $ext_href = isset($row['class'])
                        ? $this->getOuterHref(Misc::getTranslit($row['class']))
                        : $this->getOuterHref(Misc::getTranslit($name));
                    $name = strlen($name) > 30 ? mb_substr($name, 0, 22, 'UTF-8') . '&hellip;' : $name;
                    $ext_link = $ext_href
                            ? "<a class='ext_link' title='Перейти к странице' href=\"$ext_href\">
                                <img src='/modules/admin/assets/css/images/globus.png' alt='Перейти к странице'/>
                               </a>"
                            : '' ;
                    $delete_box = $row['deletable'] === '1'
                        ? <<<HTML
                        <div class="checkbox checkbox_delete" title="Удалить">
                            <input type="checkbox" name="del[{$row['id']}]"/>
                        </div>
HTML
                        : null;


                    $out .= <<<HTML
                    <li$class$id>
                        <a href='#{$page}_tab-$i'>$name</a>
                        <div class="checkbox" title="Активность"><input type="checkbox" name="act[{$row['id']}]"$checked/></div>
                        <input type="hidden" name="elems[{$row['id']}]" value="true"/>
                        $delete_box
                        $ext_link
                    </li>

HTML;

                    $out .= $this->getInnerTabsLi($row['id']);
                }
            }
        }
        return $out;
    }

    private function getInnerTabs() {
        $out = null;
        for($i = 1; $i <= count($this->data); $i++) {
            $this->opts['id'] = 'form_' . $i;
            $admin_form = fm::getInstance($this->opts, 'Admin', $this->data[$i-1]);
            $page = Router::getPageName();
            $out .= <<<HTML
                <div id="{$page}_tab-$i">
                    {$admin_form->getFormView()}
                </div>
HTML;
        }
        return $out;
    }
    
    public function getTab() {
        return <<<HTML
        <div id="inner_tabs">
            <form class='act_delete_form' data-tb='{$this->table}' action='' method='post'>
                <ul>
                    {$this->getInnerTabsLi()}
                </ul>
            {$this->paginator->getPagesHTML()}
            </form>
            {$this->getInnerTabs()}
        </div>
HTML;
    }
} 