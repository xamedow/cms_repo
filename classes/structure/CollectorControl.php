<?php
namespace structure;
use main\Router as r, main\FileHandler as fh;
/**
 * Class Collector_control
 * Включает в себя набор методов для работы с контроллером сборщика.
 * @package main
 */
class CollectorControl extends Controller {
    /**
     * Массив якорей из подключенного представления.
     * @var
     */
    private $anchors = array();

    /**
     * Присваивает массиву anchors, якоря из представления.
     * Элементы содержат массив имен модулей ($this->anchors[1]) и массив опций модулей ($this->anchors[2]).
     * @return array
     */
    public function setAnchors() {
        $pattern = '/<!--{{(\b\S+\b)}}-->(.*)<!--{{\/.*}}-->/uUsi';
        preg_match_all($pattern, $this->getView(), $this->anchors);
    }

    /**
     * Акссесор.
     * @return array
     */
    public function getAnchors() {
        return $this->anchors;
    }

    /**
     * Возвращает массив ассоциатвных массивов пар имя_модуля/опция.
     * @return array
     */
    public function getOptions() {
        $options = array_filter($this->anchors[2]);
        foreach($options as $key=>$val) {
            $options[][$this->anchors[1][$key]] = $val;
            unset($options[$key]);
        }
        return $options;
    }

    /**
     * Метод обрабатывает якоря в представлении.
     * @param bool $with_main - флаг обработки основных модулей, по умолчанию, обработка выключена
     * @throws \Exception
     */
    public function processView($with_main = false) {
        if(!empty($this->anchors[1]) && is_array($this->anchors[1])) {
            foreach ($this->anchors[1] as $key => $anchor) {
                $classname = ucfirst($anchor);
                if($with_main && $anchor === 'content' && file_exists("modules/".r::getMainModule())) {
                    $options = $this->anchors[2][$key];
                    include_once "modules/".r::getMainModule()."/controller.php";
                    $this->replaceView($this->anchors[0][$key], $main_output);
                } elseif(file_exists("classes/modules/$classname.php")) {
                    $classname = 'modules\\' . $classname;
                    $sec_mod = new $classname($this->anchors[2][$key]);
                    $this->replaceView($this->anchors[0][$key], $sec_mod->getView());
                } else {
                    throw new \Exception("Module $anchor not found ");
                }
            }
        }
    }
}