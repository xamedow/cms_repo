<?php
header('Content-Type: text/html; charset=utf-8');
# Читаем конфигурационный файл базы данных.
include_once '../classes/main/FileHandler.php';
include_once '../classes/main/Misc.php';
$db = \main\Misc::get_param(\main\FileHandler::readToArray('../conf/db'));

include_once '../classes/main/Db.php';
# Соединяемся с бд.
main\Db::init($db);
include_once '../classes/modules/Module.php';
include_once '../classes/modules/Contact.php';
$rs = new \modules\Contact('email');
$to = $rs->getView();
// Include extra form fields and/or submitter data?
// false = do not include
$extra = array(
	"form_subject"	=> true,
	"form_cc"		=> true,
	"ip"			=> true,
	"user_agent"	=> true
);

// Process
$action = isset($_POST["action"]) ? $_POST["action"] : "";
if (empty($action)) {
	// Send back the contact form HTML
    $token = smcf_token($to);
	$output = <<<HTML
    <div style='display:none'>
	<div class='contact-top'></div>
	<div class='contact-content'>
		<h1 class='contact-title'>Закажите звонок специалиста</h1>
		<div class='contact-loading' style='display:none'></div>
		<div class='contact-message' style='display:none'></div>
		<form id='wish_form' action='#' style='display:none' enctype='multipart/form-data'>
			<label for='contact-name'>*ваше имя:</label>
			<input type='text' id='contact-name' class='contact-input' name='name' tabindex='1001' />
			<label for='contact-message'>*контактный телефон:</label>
			<input type='text' id='contact-message' class='contact-input' name='phone' tabindex='1002' />
			<label for='contact-comment'>Комментарии</label>
			<textarea id='contact-comment' class='contact-input' name='comment' tabindex='1008'></textarea>
			<br/>
			<label>&nbsp;</label>
			<button type='submit' class='contact-send contact-button' tabindex='1006'>Отправить</button>
			<button type='submit' class='contact-cancel contact-button simplemodal-close' tabindex='1007'>Выход</button>
			<br/>
			<input type='hidden' name='token' value='$token'/>
		</form>
	</div>
</div>
HTML;

	echo $output;
}
else if ($action == "send") {
	// Send the email
    $subject = "Запрос на обратный звонок";
    extract($_POST);

    $body = <<<HTML
    <h2>Запрос на обратный звонок</h2>
    <table>
        <tr>
            <td style="width:200px">Имя:</td><td>$name<td/>
        </tr>
        <tr>
            <td style="width:200px">Контактный телефон:</td><td>$phone<td/>
        </tr>
        <tr>
            <td style="width:200px">Комментарии:</td><td>$comment<td/>
        </tr>
    </table>
HTML;
    $headers = 'From: info@domain.ru' . "\r\n";
    $headers.= 'MIME-Version: 1.0' . "\r\n";
    $headers.= 'Content-type: text/html;charset=utf-8' . "\r\n";

    if (mail($to, $subject, $body, $headers))	echo "<div>Ваш запрос успешно отправлен.</div>";
}

function smcf_token($s) {
	return md5("smcf-" . $s . date("WY"));
}

// Validate and send email
function smcf_send($name, $email, $subject, $message, $cc) {
	global $to, $extra;

	// Filter and validate fields
	$name = smcf_filter($name);
	$subject = smcf_filter($subject);
	$email = smcf_filter($email);
	
	// Add additional info to the message
	if ($extra["ip"]) {
		$message .= "\n\nIP: " . $_SERVER["REMOTE_ADDR"];
	}
	if ($extra["user_agent"]) {
		$message .= "\n\nUSER AGENT: " . $_SERVER["HTTP_USER_AGENT"];
	}

	// Set and wordwrap message body
	$body = "From: $name\n\n";
	$body .= "Message: $message";
	$body = wordwrap($body, 70);

	// Build header
	$headers = "From: $email\n";
	if ($cc == 1) {
		$headers .= "Cc: $email\n";
	}
	$headers .= "X-Mailer: PHP/SimpleModalContactForm";

	// UTF-8
	if (function_exists('mb_encode_mimeheader')) {
		$subject = mb_encode_mimeheader($subject, "utf-8", "B", "\n");
	}
	else {
		// you need to enable mb_encode_mimeheader or risk 
		// getting emails that are not UTF-8 encoded
	}
	$headers .= "MIME-Version: 1.0\n";
	$headers .= "Content-type: text/plain; charset=utf-8\n";
	$headers .= "Content-Transfer-Encoding: quoted-printable\n";

	// Send email
	@mail($to, $subject, $body, $headers) or 
		die("Ошибка в отправки письма сервером.");
}

// Remove any un-safe values to prevent email injection
function smcf_filter($value) {
	$pattern = array("/\n/","/\r/","/content-type:/i","/to:/i", "/from:/i", "/cc:/i");
	$value = preg_replace($pattern, "", $value);
	return $value;
}

// Validate email address format in case client-side validation "fails"
function smcf_validate_email($email) {
	$at = strrpos($email, "@");

	// Make sure the at (@) sybmol exists and  
	// it is not the first or last character
	if ($at && ($at < 1 || ($at + 1) == strlen($email)))
		return false;

	// Make sure there aren't multiple periods together
	if (preg_match("/(\.{2,})/", $email))
		return false;

	// Break up the local and domain portions
	$local = substr($email, 0, $at);
	$domain = substr($email, $at + 1);


	// Check lengths
	$locLen = strlen($local);
	$domLen = strlen($domain);
	if ($locLen < 1 || $locLen > 64 || $domLen < 4 || $domLen > 255)
		return false;

	// Make sure local and domain don't start with or end with a period
	if (preg_match("/(^\.|\.$)/", $local) || preg_match("/(^\.|\.$)/", $domain))
		return false;

	// Check for quoted-string addresses
	// Since almost anything is allowed in a quoted-string address,
	// we're just going to let them go through
	if (!preg_match('/^"(.+)"$/', $local)) {
		// It's a dot-string address...check for valid characters
		if (!preg_match('/^[-a-zA-Z0-9!#$%*\/?|^{}`~&\'+=_\.]*$/', $local))
			return false;
	}

	// Make sure domain contains only valid characters and at least one period
	if (!preg_match("/^[-a-zA-Z0-9\.]*$/", $domain) || !strpos($domain, "."))
		return false;	

	return true;
}

exit;

?>