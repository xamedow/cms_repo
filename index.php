<?php
# Определяем константы.
require_once 'conf/constants.php';
session_start();
# Подключаем автозагрузку классов.
require_once 'classes/main/Autoloader.php';

use main\Misc as mi, main\FileHandler as fh, main\ErrorHandler as e;

# Если исключения всплывают до этого уровня, отдаем браузеру начальный шаблон.
try {
# Определяем обработчик ошибок.
    e::init();
# Читаем конфигурационный файл базы данных.
    $db = mi::readFromJson(fh::readToString('conf/db.json'));
# Соединяемся с бд и выставляем кодировку.
    main\Db::init($db);
    main\Db::queryExec("SET NAMES utf8", array(), false);

# Фильтрация REQUEST массива
// Внимание! не назначть одинаковые имена аргументов в массивы GET и POST.
// использовать префиксы get_ и post_ соответственно
    if (!empty($_REQUEST)) {
        $filter = new \main\Filter($_REQUEST, 'string');
        $request = $filter->apply();
    }
    $request = null;
# Получаем маршруты.
    new \main\Router(RURI);


# Отдаем управление сборщику.
    require_once 'modules/collector/controller.php';
} catch (Exception $e) {
    e::log('common_errors', $e, true);
    echo e::dummy();
    die();
}
