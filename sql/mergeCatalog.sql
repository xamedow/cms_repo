INSERT INTO astera.mod_catalog(id, name, heading, meta_description, text, price)
SELECT old_astera.b_iblock_element.id,name, name as heading, searchable_content, detail_text, price from old_astera.b_iblock_element
left join old_astera.b_catalog_price on old_astera.b_catalog_price.PRODUCT_ID = old_astera.b_iblock_element.ID;