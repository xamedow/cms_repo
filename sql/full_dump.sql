-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.41-0ubuntu0.14.10.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.1.0.4891
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table cms.comp_modules_actions
DROP TABLE IF EXISTS `comp_modules_actions`;
CREATE TABLE IF NOT EXISTS `comp_modules_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(10) unsigned NOT NULL,
  `action_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_modules2actions_modules` (`module_id`),
  KEY `FK_modules2actions_modules_actions` (`action_id`),
  CONSTRAINT `FK_modules2actions_modules` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_modules2actions_modules_actions` FOREIGN KEY (`action_id`) REFERENCES `ref_modules_actions` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table cms.comp_orders_catalog
DROP TABLE IF EXISTS `comp_orders_catalog`;
CREATE TABLE IF NOT EXISTS `comp_orders_catalog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `catalog_id` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_comp_orders_catalog_mod_orders` (`order_id`),
  KEY `FK_comp_orders_catalog_mod_catalog` (`catalog_id`),
  CONSTRAINT `FK_comp_orders_catalog_mod_catalog` FOREIGN KEY (`catalog_id`) REFERENCES `mod_catalog` (`id`),
  CONSTRAINT `FK_comp_orders_catalog_mod_orders` FOREIGN KEY (`order_id`) REFERENCES `mod_orders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table cms.comp_related_catalog
DROP TABLE IF EXISTS `comp_related_catalog`;
CREATE TABLE IF NOT EXISTS `comp_related_catalog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `related_id` int(10) unsigned NOT NULL DEFAULT '0',
  `catalog_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK__mod_catalog` (`related_id`),
  KEY `FK__mod_catalog_2` (`catalog_id`),
  CONSTRAINT `FK__mod_catalog` FOREIGN KEY (`related_id`) REFERENCES `mod_catalog` (`id`),
  CONSTRAINT `FK__mod_catalog_2` FOREIGN KEY (`catalog_id`) REFERENCES `mod_catalog` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table cms.modules
DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0' COMMENT 'английское название модуля, совпадающие с именем папки',
  `title` varchar(50) NOT NULL DEFAULT '0' COMMENT 'русское название модуля',
  `act` int(1) unsigned NOT NULL DEFAULT '1',
  `section` int(1) unsigned DEFAULT '1' COMMENT 'номер раздела',
  `rank` int(3) unsigned NOT NULL DEFAULT '0',
  `parent` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список всех модулей проекта.';

-- Data exporting was unselected.


-- Dumping structure for table cms.modules_confmap
DROP TABLE IF EXISTS `modules_confmap`;
CREATE TABLE IF NOT EXISTS `modules_confmap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `dedicated_view` varchar(255) DEFAULT NULL,
  `common_view` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dedic_pair` (`module`,`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица сопоставления страниц и представлений.';

-- Data exporting was unselected.


-- Dumping structure for table cms.mod_admin
DROP TABLE IF EXISTS `mod_admin`;
CREATE TABLE IF NOT EXISTS `mod_admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `heading` varchar(255) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица страниц админки.';

-- Data exporting was unselected.


-- Dumping structure for table cms.mod_banner
DROP TABLE IF EXISTS `mod_banner`;
CREATE TABLE IF NOT EXISTS `mod_banner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `trash` int(1) unsigned DEFAULT '0',
  `text` text COMMENT 'Содержимое баннера',
  `rank` int(5) unsigned DEFAULT '0',
  `act` int(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table cms.mod_catalog
DROP TABLE IF EXISTS `mod_catalog`;
CREATE TABLE IF NOT EXISTS `mod_catalog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название товара',
  `heading` text COMMENT 'Заголовок товара',
  `meta_keywords` text COMMENT 'Ключи для страницы',
  `meta_title` text COMMENT 'Верхний заголовок',
  `meta_description` text COMMENT 'Описание страницы',
  `special_offer` int(1) unsigned DEFAULT NULL COMMENT 'Специальное предложение|checkbox',
  `hot_sale` int(1) unsigned DEFAULT NULL COMMENT 'Хит продаж|checkbox',
  `section_id` int(10) unsigned DEFAULT NULL COMMENT 'Категория товара|select-mod_catalog_sections',
  `text` text COMMENT 'Содержание страницы',
  `act` int(1) unsigned NOT NULL DEFAULT '1',
  `trash` int(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Trash flag',
  `rank` int(5) NOT NULL DEFAULT '0',
  `deletable` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Deletable flag',
  `price` int(11) DEFAULT NULL COMMENT 'Цена (руб)',
  `img` varchar(255) DEFAULT NULL COMMENT 'Изображение товара',
  `specs` text COMMENT 'Технические характеристики',
  `measure_in` varchar(50) DEFAULT 'шт' COMMENT 'Еденица измерения',
  `related_product` int(11) DEFAULT NULL COMMENT 'Сопутствующие товары|comp_related_catalog',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table cms.mod_catalog_sections
DROP TABLE IF EXISTS `mod_catalog_sections`;
CREATE TABLE IF NOT EXISTS `mod_catalog_sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название категории',
  `heading` text COMMENT 'Заголовок категории',
  `meta_keywords` text COMMENT 'Ключи для страницы',
  `meta_title` text COMMENT 'Верхний заголовок',
  `meta_description` text COMMENT 'Описание страницы',
  `img` varchar(255) DEFAULT NULL COMMENT 'Ярлык для категории|138*103',
  `parent` int(10) unsigned DEFAULT NULL COMMENT 'Родительская категория|select',
  `front_page` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Отображать на главной странице|checkbox',
  `text` text COMMENT 'Содержание страницы',
  `act` int(1) unsigned NOT NULL DEFAULT '1',
  `trash` int(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Trash flag',
  `rank` int(5) NOT NULL DEFAULT '0',
  `deletable` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Deletable flag',
  PRIMARY KEY (`id`),
  KEY `rank` (`rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table cms.mod_contacts
DROP TABLE IF EXISTS `mod_contacts`;
CREATE TABLE IF NOT EXISTS `mod_contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Значение',
  `type` enum('address','phone','city','email') NOT NULL DEFAULT 'phone' COMMENT 'Тип|select-mod_contacts&enum',
  `parent` int(10) unsigned DEFAULT '0' COMMENT 'Принадлежность|select-mod_contacts',
  `trash` int(1) unsigned DEFAULT '0',
  `rank` int(5) unsigned DEFAULT '0',
  `act` int(1) NOT NULL DEFAULT '1',
  `deletable` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table cms.mod_landing
DROP TABLE IF EXISTS `mod_landing`;
CREATE TABLE IF NOT EXISTS `mod_landing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `heading` varchar(255) NOT NULL COMMENT 'Заголовок раздела',
  `name` varchar(50) DEFAULT 'Главная' COMMENT 'Название раздела',
  `text` text COMMENT 'Текст раздела',
  `act` int(1) unsigned NOT NULL DEFAULT '1',
  `rank` int(4) unsigned NOT NULL,
  `class` varchar(50) DEFAULT NULL,
  `trash` int(1) unsigned NOT NULL DEFAULT '0',
  `deletable` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table cms.mod_lk
DROP TABLE IF EXISTS `mod_lk`;
CREATE TABLE IF NOT EXISTS `mod_lk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `act` int(11) DEFAULT NULL,
  `name` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table cms.mod_menu
DROP TABLE IF EXISTS `mod_menu`;
CREATE TABLE IF NOT EXISTS `mod_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT 'Название элемента в меню',
  `module_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Модуль|select-modules',
  `page_name` varchar(50) NOT NULL COMMENT 'Имя страницы',
  `menu_mode` varchar(50) DEFAULT '0' COMMENT 'Тип меню',
  PRIMARY KEY (`id`),
  UNIQUE KEY `page_module` (`page_name`,`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Страницы находящиеся в меню.';

-- Data exporting was unselected.


-- Dumping structure for table cms.mod_meta
DROP TABLE IF EXISTS `mod_meta`;
CREATE TABLE IF NOT EXISTS `mod_meta` (
  `id` int(10) NOT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT 'Основной заголовок',
  `keywords` varchar(1024) DEFAULT NULL COMMENT 'Основные ключевые слова',
  `description` varchar(1024) DEFAULT NULL COMMENT 'Основное описание',
  `trash` int(1) unsigned DEFAULT '1',
  `rank` int(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table cms.mod_orders
DROP TABLE IF EXISTS `mod_orders`;
CREATE TABLE IF NOT EXISTS `mod_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `act` int(1) unsigned NOT NULL DEFAULT '1',
  `trash` int(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Trash flag',
  `rank` int(5) NOT NULL DEFAULT '0',
  `deletable` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Deletable flag',
  `client_id` int(10) unsigned DEFAULT NULL COMMENT 'Клиент|select-mod_static',
  PRIMARY KEY (`id`),
  KEY `rank` (`rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table cms.mod_pastebin
DROP TABLE IF EXISTS `mod_pastebin`;
CREATE TABLE IF NOT EXISTS `mod_pastebin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table cms.mod_rest
DROP TABLE IF EXISTS `mod_rest`;
CREATE TABLE IF NOT EXISTS `mod_rest` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название страницы',
  `heading` text COMMENT 'Заголовок страницы',
  `meta_keywords` text COMMENT 'Ключи для страницы',
  `meta_title` text COMMENT 'Верхний заголовок',
  `meta_description` text COMMENT 'Описание страницы',
  `in_menu` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Показывать в меню|checkbox',
  `text` text COMMENT 'Содержание страницы',
  `act` int(1) unsigned NOT NULL DEFAULT '1',
  `trash` int(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Trash flag',
  `rank` int(5) NOT NULL DEFAULT '0',
  `deletable` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Deletable flag',
  `parent` int(10) unsigned DEFAULT NULL COMMENT 'Родительская страница|select-mod_static',
  PRIMARY KEY (`id`),
  KEY `rank` (`rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table cms.mod_service
DROP TABLE IF EXISTS `mod_service`;
CREATE TABLE IF NOT EXISTS `mod_service` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название страницы',
  `heading` text COMMENT 'Заголовок страницы',
  `preview_text` text COMMENT 'Анонс на главной странице',
  `meta_keys` text COMMENT 'Ключи для страницы',
  `meta_description` text COMMENT 'Описание страницы',
  `meta_title` text COMMENT 'Верхний заголовок',
  `date_from` date DEFAULT NULL COMMENT 'Дата начала мероприятия',
  `date_due` date DEFAULT NULL COMMENT 'Дата окончания мероприятия',
  `img` varchar(255) DEFAULT NULL COMMENT 'Изображение на главной странице',
  `front_page` int(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Отображать на главной|checkbox',
  `text` text COMMENT 'Содержание страницы',
  `act` int(1) unsigned NOT NULL DEFAULT '1',
  `trash` int(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Trash flag',
  `rank` int(5) unsigned NOT NULL DEFAULT '0',
  `deletable` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Deletable flag',
  `parent` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Родительская страница|select-mod_service',
  PRIMARY KEY (`id`),
  KEY `rank` (`rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table cms.mod_static
DROP TABLE IF EXISTS `mod_static`;
CREATE TABLE IF NOT EXISTS `mod_static` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название страницы',
  `heading` text COMMENT 'Заголовок страницы',
  `meta_keywords` text COMMENT 'Ключи для страницы',
  `meta_title` text COMMENT 'Верхний заголовок',
  `meta_description` text COMMENT 'Описание страницы',
  `in_menu` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Показывать в меню|checkbox',
  `text` text COMMENT 'Содержание страницы',
  `act` int(1) unsigned NOT NULL DEFAULT '1',
  `trash` int(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Trash flag',
  `rank` int(5) NOT NULL DEFAULT '0',
  `deletable` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Deletable flag',
  `parent` int(10) unsigned DEFAULT NULL COMMENT 'Родительская страница|select-mod_static',
  PRIMARY KEY (`id`),
  KEY `rank` (`rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table cms.mod_users
DROP TABLE IF EXISTS `mod_users`;
CREATE TABLE IF NOT EXISTS `mod_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Логин',
  `name` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Имя',
  `pass` varchar(32) NOT NULL DEFAULT '0' COMMENT 'Пароль (введите новое значение, чтобы изменить пароль)',
  `session_hash` varchar(32) NOT NULL DEFAULT '0',
  `trash` int(1) unsigned NOT NULL DEFAULT '0',
  `rank` int(5) unsigned NOT NULL DEFAULT '0',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Группа|select-user_groups',
  `act` int(1) NOT NULL DEFAULT '1',
  `deletable` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ_login` (`login`),
  KEY `FK_users_user_groups` (`group_id`),
  KEY `rank` (`rank`),
  CONSTRAINT `FK_users_user_groups` FOREIGN KEY (`group_id`) REFERENCES `ref_user_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table cms.ref_clients
DROP TABLE IF EXISTS `ref_clients`;
CREATE TABLE IF NOT EXISTS `ref_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Имя клиента',
  `pass` varchar(32) NOT NULL DEFAULT '0' COMMENT 'Пароль (введите новое значение, чтобы изменить пароль)',
  `login` varchar(32) NOT NULL DEFAULT '0',
  `phone` varchar(32) NOT NULL DEFAULT '0',
  `address` varchar(32) NOT NULL DEFAULT '0',
  `session_hash` varchar(32) NOT NULL DEFAULT '0',
  `act` int(1) unsigned NOT NULL DEFAULT '0',
  `rank` int(10) unsigned NOT NULL DEFAULT '0',
  `deletable` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table cms.ref_modules_actions
DROP TABLE IF EXISTS `ref_modules_actions`;
CREATE TABLE IF NOT EXISTS `ref_modules_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '0',
  `rank` int(3) unsigned NOT NULL DEFAULT '0',
  `type` varchar(50) DEFAULT 'list',
  `table` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table cms.ref_routes
DROP TABLE IF EXISTS `ref_routes`;
CREATE TABLE IF NOT EXISTS `ref_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `income` varchar(255) NOT NULL,
  `outcome` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ind_income` (`income`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица редиректов и маршрутизации.';

-- Data exporting was unselected.


-- Dumping structure for table cms.ref_user_groups
DROP TABLE IF EXISTS `ref_user_groups`;
CREATE TABLE IF NOT EXISTS `ref_user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `access_rights` int(15) unsigned NOT NULL DEFAULT '0',
  `rank` int(10) unsigned DEFAULT NULL,
  `act` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
