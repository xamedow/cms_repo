<?php
use main\Router as r;
$collector_model = new structure\Model(r::getMainModule(), 'main', r::getPageName());
$view = '';
try {
    $view = $collector_model->getView();
} catch (Exception $e) {
    main\ErrorHandler::log('common_errors', $e);
}