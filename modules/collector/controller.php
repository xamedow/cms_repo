<?php

try {
    require_once 'model.php';
    $mod_collector = new structure\CollectorControl('collector', 'main');
    $mod_collector->setView($view);
    $mod_collector->readView();
    $mod_collector->setAnchors();

# Подключаем модули и обрабатываем представление.
    $mod_collector->processView(true);

# Проверяем якори модулей, не входящие в основное представление, например из БД.
    $mod_collector->setView($mod_collector->getView());
    $mod_collector->setAnchors();
    $mod_collector->processView();

# Выводим сформированное представление.
    echo $mod_collector->getView();
} catch (Exception $e) {
    main\ErrorHandler::log('collector_errors', $e, true);
}