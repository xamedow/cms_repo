<?php
$catalog_model = new structure\CatalogModel('catalog', 'main');
$data['products'] = $catalog_model->getProducts();
$data['section'] = current( $catalog_model->getSectionContent() );
if( empty($data['products'][0]['id']) ) {
    $data['product'] = current( $catalog_model->getContent('id') );
}
