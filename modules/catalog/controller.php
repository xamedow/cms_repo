<?php
require_once 'model.php';
# Если страница модуля не найдена.
if(!$data) {
    header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
    header("Location: http://" . HHOST . "/static/404.html");
}
$ctrl = new structure\CatalogController('catalog', 'main');
$ctrl->setView($options);
$out = null;
if(!empty($data['product'])) {
    $out = $ctrl->processProductCard($data['product']);
} elseif(!empty($data['section'])) {
    $section_name = empty($data['section']['heading']) ? $data['section']['name'] : $data['section']['heading'];
    $products_list = empty($data['products'][0]['id']) ? 'Товаров в категории пока нет' : $data['products'];
    $out = $ctrl->processProductsList($products_list, 'products_list');
    $out = $ctrl->processProductsList($section_name, 'heading', $out);
    $out = $ctrl->processProductsList($data['section']['text'], 'text', $out);
} else {
    header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
    header("Location: http://" . HHOST . "/static/404.html");
}
$ctrl->replaceView('data', $out, false);

$main_output = $ctrl->getView();
