<?php
require_once 'model.php';
$ctrl = new structure\lkController('lk', 'main');


$ctrl->setView($options);
$out = null;

new modules\admin\AjaxHandler(array(), 'ref_clients');

if(!empty($_SESSION['ref_clients_id'])) {
    $ctrl->init($_SESSION['ref_clients_id']);
    $orders = $ctrl->getOrders() ? $ctrl->getOrders() : '<div>Список заказов пуст.</div>';
    $ctrl->replaceView('order_list', $orders, false);
    $ctrl->replaceView('credentials', $ctrl->getCreds(), false);
}

$main_output = $ctrl->getView();
