<?php
namespace Main;

$parts = Router::getUrlParts();
//list($a, $b, $action, $module, $id) = $parts;
$action = $parts[2];
$module = $parts[3];
$id = isset($parts[4]) ? $parts[4] : null;


function getRestData($module, $id) {
    $subquery = $fields = null;
    $subarray = array();
    $options_file = file_exists(DROOT . "/modules/$module/options.json")
        ? DROOT . "/modules/$module/options.json"
        : DROOT . "/modules/admin/modules_options/$module.json";
    $module_arr = (array)Misc::readFromJson(FileHandler::readToString($options_file));
    if(!empty($id)) {
        $subquery = strpos($id, ',') !== false
            ?   " LIMIT $id"
            :   " WHERE id = $id";
        $subarray = array(':id', $id);
        $fields = '*';
    } else {
        $fields_arr = $module_arr['output_options'][$module]['default_fields'];

        foreach ($fields_arr as $field) {
            $fields .= ", {$field['name']}";
        }
        $fields = ltrim($fields, ',');
    }
    $table = isset($module_arr['sql']['main_table']) ? $module_arr['sql']['main_table'] : "mod_$module";
    $query = "SELECT $fields FROM $table$subquery";
    FileHandler::writeTemp($query);
    return Db::queryExec($query, $subarray);
}

$func_name = $action . 'RestData';
$data = call_user_func_array(__NAMESPACE__ . '\\' . $func_name, array($module, $id));
$main_output = json_encode($data);
