<?php
$data = $data2 = $data3 = '';
# Пробуем подключить модель.
try {
    require_once 'model.php';
} catch (PDOException $e) {
    main\ErrorHandler::log('db_errors', $e, true);
} catch (Exception $e) {
    main\ErrorHandler::log('file_errors', $e, true);
}

# Инициализуруем контроллер.
$mod_admin = new structure\Controller('admin', 'main');

# Считываем представление, $options из модуля collector.
$mod_admin->setView($options);
# Обрабатываем представление.
// TODO make this a method.
if (\main\Router::getPageName()) {
    //$mod_admin->replaceView('h1', $h1, false);
    //$mod_admin->replaceView('h_tabs_li', $h_tabs_li, false);
    //$mod_admin->replaceView('h_tabs_div', $h_tabs_div, false);
    //$mod_admin->replaceView('table', 'mod_'.\main\Router::getPageName(), false);
} elseif(isset($h1)) {
    $mod_admin->replaceView('h1', $h1, false);
}

# Отдаем представление в модуль сборщика.
$main_output = $mod_admin->getView();
