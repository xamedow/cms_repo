<?php
use main\Db as db;
use main\Router as r;

if (r::getPageName()) {
    $admin_model = new structure\AdminModel('admin', 'main', r::getPageName());
    $cur_mod = current(db::queryExec("SELECT title FROM modules WHERE name=:name", array(':name' => r::getPageName())));
    $h1 = $cur_mod['title'];

    $h_tabs_li = $admin_model->getTabsLi();
    $h_tabs_div = $admin_model->getTabsDiv();

} else {
    if (isset($_SESSION['login_error'])) {
        $h1 = $_SESSION['login_error'];
        session_destroy();
    }
}
new \modules\admin\AjaxHandler(array('sm'=>array(1920,1080)));