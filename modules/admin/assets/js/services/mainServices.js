/**
 * Created by Андрей on 14.12.2014.
 */
var getPromise = function (q, http, url) {
        var defer = q.defer();
        http.get(url)
            .success(function (data) {
                defer.resolve(data);
            });
        return defer.promise;
    },

    /**
     * Сервис для настроек модуля
     * @param $q
     * @param $http
     * @returns {{getOptions: Function}}
     */
    optionsService = function($q, $http) {
        var getOptions = function () {
            return getPromise($q, $http, '/modules/admin/assets/js/data/catalog_options.json');
        };
        return {getOptions: getOptions};

    },
	
    /**
     * Сервис для контента
     * @param $q
     * @param $http
     * @returns {{getContent: Function}}
     */
    contentService = function($q, $http) {
        var getContent = function (module) {
            return getPromise($q, $http, '/rest/get/' + module);
        };
        return {getContent: getContent};
    },

    /**
     * Сервис для списка модулей
     * @param $q
     * @param $http
     * @returns {{getModules: Function}}
     */
    modulesService = function($q, $http) {
        var getModules = function () {
            return getPromise($q, $http, '/modules/admin/assets/js/data/modules.json');
        };
        return {getModules: getModules};
    };
	
angular.module('mainServices', [])
	.factory('OptionsService', ['$q', '$http', optionsService])
	.factory('ContentService', ['$q', '$http', contentService])
	.factory('ModulesService', ['$q', '$http', modulesService]);
