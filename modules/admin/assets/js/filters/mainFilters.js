(function () {
    var columnsOrder = function () {
        return function (items, direction) {
            var ascsorter = function (a, b) {
                    return a.name > b.name
                        ? -1
                        : a.name < b.name
                        ? 1
                        : 0;
                },
                descsorter = function (a, b) {
                    return a.name > b.name
                        ? 1
                        : a.name < b.name
                        ? -1
                        : 0;
                };
            return items ? direction ? items.sort(descsorter) : items.sort(ascsorter) : null;
        };
    },
        validatePage = function (page, maxPages) {
            page = Number(page);
            return !page ? 1 : page > maxPages ? maxPages : page;
        },
        getMaxPages = function (items, pageSize) {
            return Math.ceil(items.length / pageSize);
        },
        paginator = function ($filter) {
            return function(items, curPage, pageSize) {
                pageSize = parseInt(pageSize);
                curPage = validatePage(parseInt(curPage), getMaxPages(items, pageSize));

                if(angular.isArray(items) && angular.isNumber(curPage) && angular.isNumber(pageSize)) {
                    var start = (curPage - 1) * pageSize;
                    return $filter('limitTo')(items.splice(start), pageSize);
                }
                return items;
            }
        },
        paginationCount = function () {
            return function(items, curPage, pageSize) {
                pageSize = parseInt(pageSize);
                curPage = validatePage(parseInt(curPage), getMaxPages(items, pageSize));
                if(angular.isArray(items)) {
                    var out = [],
                        i = 1,
                        l = getMaxPages(items, pageSize),
                        showArr = [curPage - 2, curPage - 1, curPage, curPage + 1, curPage + 2];
                    if(curPage < 3) {
                        showArr = [1, 2, 3, 4, 5];
                    } else if(curPage > l - 2) {
                        showArr = [l, l - 1, l - 2, l - 3, l - 4];
                    }
                    for(; i <= l; ++i) {
                        if (showArr.indexOf(i) > -1) {
                            out.push(i);
                        }
                    }
                    return out;
                }
                return items;
            }
        };
    angular.module('mainFilters', [])
        .filter('customColumnOrder', columnsOrder)
        .filter('paginator', paginator)
        .filter('paginationCount', paginationCount);
})();