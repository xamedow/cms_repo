var paginatorCtrl = function ($scope) {
    var validatePage = function (page, maxPages) {
        page = Number(page);
        return !page ? 1 : page > maxPages ? maxPages : page;
    };


    $scope.model.currentPage = 1;
    $scope.isCurrentPage = function(page) {
        return validatePage($scope.model.currentPage, $scope.getMaxPage()) == page;
    };

    $scope.getMaxPage = function () {
        if($scope.$parent.model.contentItems) {
            return Math.ceil($scope.$parent.model.contentItems.length / $scope.$parent.currentModule.items_on_page);
        }
        return 0;
    };

    $scope.setCurrentPage = function (page) {
        $scope.model.currentPage = validatePage(page, $scope.getMaxPage());
    }
};

angular.module('control-panel').controller('paginatorCtrl', ['$scope', 'OptionsService', 'ModulesService', 'ContentService', paginatorCtrl]);