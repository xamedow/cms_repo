(function () {

    var mainCtrl = function ($scope, OptionsService, ModulesService, ContentService) {

        var temp = [
                {
                    "actions": [
                        {
                            "name": "products",
                            "title": "Список товаров",
                            "template": "products_list"
                        },
                        {
                            "name": "sections",
                            "title": "Список категорий",
                            "template": "sections_list"
                        },
                        {
                            "name": "settings",
                            "title": "Настройки",
                            "template": "settings"
                        }
                    ],
                    "items": [
                        {
                            "name": "\u04211",
                            "act": true,
                            "section": "\u0421\u0432\u0430\u0434\u0435\u0431\u043d\u044b\u0435 \u0442\u043e\u0440\u0442\u044b"
                        },
                        {
                            "name": "\u04212",
                            "act": true,
                            "section": "\u0421\u0432\u0430\u0434\u0435\u0431\u043d\u044b\u0435 \u0442\u043e\u0440\u0442\u044b"
                        },
                        {
                            "name": "\u04213",
                            "act": true,
                            "section": "\u0421\u0432\u0430\u0434\u0435\u0431\u043d\u044b\u0435 \u0442\u043e\u0440\u0442\u044b"
                        }
                    ]
                },
                {
                    "actions": [
                        {
                            "name": "products",
                            "title": "Список услуг",
                            "template": "products_list"
                        },
                        {
                            "name": "sections",
                            "title": "Список категорий",
                            "template": "sections_list"
                        },
                        {
                            "name": "settings",
                            "title": "Настройки",
                            "template": "settings"
                        }
                    ],
                    "items": [
                        {
                            "name": "\u04214",
                            "act": true,
                            "section": "\u0421\u0432\u0430\u0434\u0435\u0431\u043d\u044b\u0435 \u0442\u043e\u0440\u0442\u044b"
                        },
                        {
                            "name": "\u04215",
                            "act": true,
                            "section": "\u0421\u0432\u0430\u0434\u0435\u0431\u043d\u044b\u0435 \u0442\u043e\u0440\u0442\u044b"
                        },
                        {
                            "name": "\u04216",
                            "act": true,
                            "section": "\u0421\u0432\u0430\u0434\u0435\u0431\u043d\u044b\u0435 \u0442\u043e\u0440\u0442\u044b"
                        }
                    ]
                },
                {
                    "actions": [
                        {
                            "name": "products",
                            "title": "Список изображений",
                            "template": "products_list"
                        },
                        {
                            "name": "sections",
                            "title": "Список категорий",
                            "template": "sections_list"
                        },
                        {
                            "name": "settings",
                            "title": "Настройки",
                            "template": "settings"
                        }
                    ],
                    "items": [
                        {
                            "name": "\u04217",
                            "act": true,
                            "section": "\u0421\u0432\u0430\u0434\u0435\u0431\u043d\u044b\u0435 \u0442\u043e\u0440\u0442\u044b"
                        },
                        {
                            "name": "\u04218",
                            "act": true,
                            "section": "\u0421\u0432\u0430\u0434\u0435\u0431\u043d\u044b\u0435 \u0442\u043e\u0440\u0442\u044b"
                        },
                        {
                            "name": "\u04219",
                            "act": true,
                            "section": "\u0421\u0432\u0430\u0434\u0435\u0431\u043d\u044b\u0435 \u0442\u043e\u0440\u0442\u044b"
                        }
                    ]
                },
                {
                    "actions": [
                        {
                            "name": "products",
                            "title": "Список изображений123",
                            "template": "products_list"
                        },
                        {
                            "name": "sections",
                            "title": "Список категорий",
                            "template": "sections_list"
                        },
                        {
                            "name": "settings",
                            "title": "Настройки",
                            "template": "settings"
                        }
                    ],
                    "items": [
                        {
                            "name": "\u042114",
                            "act": true,
                            "section": "\u0421\u0432\u0430\u0434\u0435\u0431\u043d\u044b\u0435 \u0442\u043e\u0440\u0442\u044b"
                        },
                        {
                            "name": "\u042115",
                            "act": true,
                            "section": "\u0421\u0432\u0430\u0434\u0435\u0431\u043d\u044b\u0435 \u0442\u043e\u0440\u0442\u044b"
                        }
                    ]
                }
            ],
            convertBools = function (data) {
                var i = data.length;
                for (; i >= 0; --i) {
                    if (data[i]) {
                        data[i].act = !!data[i].act;
                    }
                }
                return data;
            },
            checkAllLabel = "Включить",
            uncheckAllLabel = "Выключить",
            checkAllFlag = true,
            setActField = function (flag, items, page, onPage) {
                if (items) {
                    var i = page ? (page * onPage) - 1 : items.length,
                        end = page ? ((page - 1) * onPage) : 0;

                    for (; i >= end; --i) {
                        if (items[i] !== undefined && items[i]['act'] !== undefined) {
                            items[i]['act'] = flag;
                        }
                    }
                }
                return items;
            };

        // Fields.
        $scope.model = {};
        $scope.filterDirection = true;
        $scope.searchString = '';
        //$scope.model.actions = temp[0].actions;
        //$scope.currentAction = temp[0].actions[0];
        $scope.checkAllLabel = checkAllLabel;


        $scope.currentActionTemplate = '/modules/admin/assets/js/views/' + temp[0].actions[0].template + '.html';

        // Deffered data.
        /*
         OptionsService.getOptions().then(function (data) {
         $scope.model.actions = data.actions;
         $scope.currentAction = data.actions[0];

         });
         */

        ModulesService.getModules().then(function (data) {
            $scope.modules = data;
            $scope.currentModule = data[0];
            $scope.currentPill = $scope.currentModule.pills[0];
            // TODO duplicate
            ContentService.getContent($scope.currentModule.name).then(function (data) {
                data = convertBools(data);
                $scope.model.contentItems = data;
                // Items on page
                $scope.model.currentItemsCount = $scope.model.contentItems.length;
                $scope.model.itemsOnPageInterval = [$scope.model.currentItemsCount,100,50,25,10];
            });

        });


        // Actions.
        $scope.isCurrentAction = function (name) {
            return name === $scope.currentAction.name;
        };
        $scope.setCurrentAction = function (action) {
            $scope.currentAction = action;
            $scope.currentActionTemplate = 'js/views/' + action.template + '.html';
        };


        $scope.setCurrentContents = function (module) {
            $scope.currentModule = module;
            // TODO duplicate
            $scope.currentPill = $scope.currentModule.pills[0];
            ContentService.getContent($scope.currentModule.name).then(function (data) {
                data = convertBools(data);
                $scope.model.contentItems = data;
                // Items on page
                $scope.model.currentItemsCount = $scope.model.contentItems.length;
                $scope.model.itemsOnPageInterval = [$scope.model.currentItemsCount,100,50,25,10];
            });

        };


        // Methods.
        $scope.changeFilterDirection = function () {
            $scope.filterDirection = !$scope.filterDirection;
        };

        $scope.toggleFilter = function () {
            $scope.filter_show = !$scope.filter_show;
        };

        $scope.isCurrentModule = function (module) {
            return module === $scope.currentModule;
        };


        $scope.addContentItem = function (itemName) {
            $scope.model.contentItems.push({name: itemName, act: false});
        };

        $scope.checkAll = function (page, onPage) {
            $scope.checkAllLabel = checkAllFlag ? checkAllLabel : uncheckAllLabel;
            checkAllFlag = !checkAllFlag;
            $scope.model.contentItems = setActField(checkAllFlag, $scope.model.contentItems, page, onPage);
        };

        $scope.checkOne = function (act) {
            checkAllFlag = checkAllFlag && act;
        };

        // Main pills
        $scope.isCurrentPill = function (pill) {
            return $scope.currentPill == pill;
        };

        $scope.setCurrentPill = function (pill) {
            $scope.currentPill = pill;
        };

    };

    angular.module('control-panel').controller('mainCtrl', ['$scope', 'OptionsService', 'ModulesService', 'ContentService', mainCtrl]);
})();