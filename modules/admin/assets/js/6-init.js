$(function () {
    $('a.adminLogout').on('click', function (event) {
		console.log('adminLogout click');
		$.ajax({
			url: '',
			type: 'post',
			data: {
				action: 'exit'
			},
			success: function () {
				console.log('ajax success');
				window.location = '/admin/';
			}
		});
    });
});
