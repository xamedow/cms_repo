<?php
require_once 'model.php';
# Если страница модуля не найдена.
if(!$data) {
    header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
    \main\Router::redirect("/static/404.html");
}
$mod_content = new structure\Controller('static', 'main');
$mod_content->setView($options);
$mod_content->replaceView('text', $data['text'], false);
$mod_content->replaceView('heading', "{$data['heading']}", false);

$main_output = $mod_content->getView();
	
