<!--{{head}}--><!--{{/head}}-->
<body>
<header>
    <div>
        <a href="/" id="logo">интернет-магазин строительных <br>и отделочных материалов</a>

        <div id="bucket">
            <a href="">Корзина заказа</a>
            Ваша корзина пока пустая
            <div>Зайдите в каталог, затем добавьте в корзину нужный вам товар.</div>
        </div>
        <!--{{userHQ}}-->
        <form action="/" method="post">
            <a href="" class="registr">Регистрация</a>
            <a href="" class="forget_password">Забыли пароль</a>
            <input class="login" name="login" type="text"/>
            <input class="password" name="pass" type="text"/>
            <input type="submit" name="auth_form" value="Войти"/>
        </form>
        <!--{{/userHQ}}-->
        <div class="phone">
            (8452) <span>46-46-74</span><br>
            <a href="#" class="call_me">закажите обратный звонок</a>
        </div>
        <div class="consultant">
            Консультант Online<br>
            <a href="">мы поможем вам с выбором</a>
        </div>
        <div>
</header>
<nav class="clearfix">
    <a href="#" id="pull">Меню</a>
    <ul class="clearfix">
        <!--{{menu}}-->static<!--{{/menu}}-->
    </ul>
</nav>
<div id="wrapper">
<div id="banners">
    <div class="banner_image">
        <img src="/images/banner.jpg" alt="">
        <div class="banner_text">Небольшой текст о нашем <br> горячем предложении</div>
    </div>
    <ul>
        <li><span>Горячее предложение</span></li>
        <li><a href="">Доставим бесплатно</a></li>
        <li><a href="">Во сколько вам нужно?</a></li>
        <li><a href="">Поднимем на этаж</a></li>
    </ul>
</div>
<!--{{scroll}}-->
<div id="scroll">
    <h2>Лидеры продаж</h2>
    <a class="buttons prev" href="#"></a>
    <div class="viewport">
        <ul class="overview">
            <li>
                <a href="">
                    <div class="image">
                        <img src="/images/scroll.png" alt="">
                    </div>
                    <div class="product_name">Фундаменты ленточные (ФЛ)</div>
                    <div class="price">от 1 080 руб</div>
                    <div class="specify">подробнее</div>
                </a>
            </li>
        </ul>
    </div>
    <a class="buttons next" href="#"></a>
</div>
<!--{{/scroll}}-->
<!--{{search}}-->
<form id="searchbar" action="#" method="post">
    <input type="text" placeholder="Поиск стройматериалов" name="search_text" id="search_text">
    <input type="submit" value="найти" name="search_submit"/>
</form>
<!--{{/search}}-->
<div id="catalog">
<h2>Каталог строительных и отделочных материалов</h2>
<ul>
<!--{{preview}}-->catalog_sections<!--{{/preview}}-->
</ul>
</div>
<div id="content">
    <!--{{content}}--><h1><!--{{heading}}--></h1><!--{{text}}--><!--{{/content}}-->
    <a href="/static/o_magazine.html" class="specify">подробнее</a>
</div>
</div>
<footer>
    <div id="footer_center">
        <ul id="footer_menu">
            <!--{{menu}}-->static<!--{{/menu}}-->
        </ul>
        <div class="info">«Астера» - интернет-магазин по продаже строительных материалов. Не является публичной
            офертой
        </div>
        <div class="phone">+7 (8452) <!--{{contact}}-->phone<!--{{/contact}}--></div>
        <a href="" class="callback call_me">заказать обратный звонок</a>

        <div class="question">Остались вопросы?</div>
        <a href="" class="consultant">напишите нам сейчас</a>

        <div class="copyright">
            Copyright &copy; 2012-2014 «Астера»<br>
            <a href="/">Строительные и отделочные материалы саратов</a>
        </div>
        <ul class="counter">
            <!--{{banner}}-->Счетчики<!--{{/banner}}-->
        </ul>
        <div class="developer">
            <a href="http://www.cherepkova.ru">Разработка сайта</a><br>
            Дизайн-студия Антонины Черепковой
        </div>
</footer>
</div>
<a href='#' id='Go_Top'  class="hide">НАВЕРХ</a>
</body>
</html>