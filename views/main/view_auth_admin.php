<!--{{head}}-->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <!--{{styles}}-->
    <link type="text/css" rel="stylesheet"
          href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css"/>
    <meta name="keywords" content="<!--{{keys}}-->">
    <meta name="description" content="<!--{{description}}-->">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <title><!--{{title}}--></title>
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/jquery-migrate/jquery-migrate.min.js"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/bower_components/angularjs/angular.js"></script>
    <!--{{scripts}}-->
</head>
<body>
<!--{{/head}}-->
<div>
    <!--BEGIN BACK TO TOP-->
    <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
    <!--END BACK TO TOP-->
    <!--BEGIN TOPBAR-->
    <div id="header-topbar-option-demo" class="page-header-topbar">
        <nav id="topbar" role="navigation" style="margin-bottom: 0;" data-step="3"
             class="navbar navbar-default navbar-static-top">
            <div class="navbar-header">
                <a id="logo" href="index.html" class="navbar-brand">
                    <span class="fa fa-rocket"></span>
                    <span class="logo-text">Cherry-CMS</span>
                </a>
            </div>
            <div class="topbar-main">
            </div>
        </nav>
    </div>
    <!--END TOPBAR-->
    <div id="wrapper">
        <!--BEGIN CONTENT-->
        <div class="page-content">
            <div id="tab-general">
                <div class="row mbl">
                    <!--{{content}}-->
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="panel panel-red">
                                    <div class="panel-heading">
                                        Авторизация
                                    </div>
                                    <div class="panel-body pan">
                                        <form action="#" method="post" class="form-horizontal">
                                            <div class="form-body pal">
                                                <div class="form-group">
                                                    <label for="inputName" class="col-md-3 control-label">
                                                        Логин</label>

                                                    <div class="col-md-9">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-user"></i>
                                                            <input id="inputName" name="login" type="text"
                                                                   placeholder="" required="required"
                                                                   class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputPassword" class="col-md-3 control-label">
                                                        Пароль</label>

                                                    <div class="col-md-9">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-lock"></i>
                                                            <input id="inputPassword" type="password" placeholder=""
                                                                   name="pass" required="required"
                                                                   class="form-control"/>
                                                        </div>
                                                    <span class="help-block mbn"><a href="#">
                                                            <small>Забыли пароль?</small>
                                                        </a></span>
                                                    </div>
                                                </div>
                                                <div class="form-group mbn">
                                                    <div class="col-md-offset-3 col-md-6">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input tabindex="5" type="checkbox" name="remember" value="true"/>&nbsp; Не выходить
                                                                из системы</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions text-right pal">
                                                <button type="submit" name="auth_form" class="btn btn-danger">
                                                    Войти
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--{{/content}}-->
                </div>
            </div>
        </div>
        <!--END CONTENT-->
        <!--BEGIN FOOTER-->
        <div id="footer">
            <div class="copyright">
                <a href="http://cherepkova.ru">2015 © cherryDevs.ru</a></div>
        </div>
        <!--END FOOTER-->
    </div>
    <!--END PAGE WRAPPER-->
</div>
</body>
</html>