<!--{{head}}--><!--{{/head}}-->
<body>
<header>
    <div>
        <a href="/" id="logo">интернет-магазин строительных <br>и отделочных материалов</a>

        <div id="bucket">
            <a href="">Корзина заказа</a>
            Ваша корзина пока пустая
            <div>Зайдите в каталог, затем добавьте в корзину нужный вам товар.</div>
        </div>
        <!--{{userHQ}}-->
        <form action="/" method="post">
            <a href="" class="registr">Регистрация</a>
            <a href="" class="forget_password">Забыли пароль</a>
            <input class="login" name="login" type="text"/>
            <input class="password" name="pass" type="text"/>
            <input type="submit" name="auth_form" value="Войти"/>
        </form>
        <!--{{/userHQ}}-->
        <div class="phone">
            (8452) <span>46-46-74</span><br>
            <a href="#" class="call_me">закажите обратный звонок</a>
        </div>
        <div class="consultant">
            Консультант Online<br>
            <a href="">мы поможем вам с выбором</a>
        </div>
        <div>
</header>
<nav class="clearfix">
    <a href="#" id="pull">Меню</a>
    <ul class="clearfix">
        <!--{{menu}}-->static<!--{{/menu}}-->
    </ul>
</nav>
<div id="wrapper">
    <div id="content">
        <!--{{content}}-->
        <!--{{/content}}-->
        <div class="whiteout">
            <form class="reg_init" action="">
                <h2>Регистрация</h2>
                <ul>
                    <li><label for="new_user_name">Ваше ФИО или название организации</label>
                        <input required="required" id="new_user_name" name="new_user_name" type="text"/></li>
                    <li><label for="new_user_email">Электронная почта</label>
                        <input required="required" id="new_user_email" name="new_user_email" type="text"/></li>
                    <li><label for="new_user_phone">Контактный телефон</label>
                        <input required="required" id="new_user_phone" name="new_user_phone" type="text"/></li>
                </ul><ul>
                    <li><label for="new_user_address">Адрес доставки</label>
                        <input required="required" id="new_user_address" name="new_user_address" type="text"/></li>
                    <li><!--{{captcha}}--><!--{{/captcha}}--></li>
                    <li><label for="new_user_captcha">Защитный код</label>
                        <input required="required" id="new_user_captcha" name="new_user_captcha" type="text"/></li>
                </ul>
                <input type="hidden" name="action" value="new"/>
                <p></p>
                <input type="submit" value="Зарегистрироваться"/>
            </form>
        </div>
    </div>
</div>

<footer>
    <div id="footer_center">
        <ul id="footer_menu">
            <!--{{menu}}-->static<!--{{/menu}}-->
        </ul>
        <div class="info">«Астера» - интернет-магазин по продаже строительных материалов. Не является публичной
            офертой
        </div>
        <div class="phone">+7 (8452) <!--{{contact}}-->phone<!--{{/contact}}--></div>
        <a href="" class="callback call_me">заказать обратный звонок</a>

        <div class="question">Остались вопросы?</div>
        <a href="" class="consultant">напишите нам сейчас</a>

        <div class="copyright">
            Copyright &copy; 2012-2014 «Астера»<br>
            <a href="/">Строительные и отделочные материалы саратов</a>
        </div>
        <ul class="counter">
            <!--{{banner}}-->Счетчики<!--{{/banner}}-->
        </ul>
        <div class="developer">
            <a href="http://www.cherepkova.ru">Разработка сайта</a><br>
            Дизайн-студия Антонины Черепковой
        </div>
</footer>
</div>
<a href='#' id='Go_Top'  class="hide">НАВЕРХ</a>
</body>
</html>