<div class="product_card">
    <h2><!--{{heading}}--></h2>
    <img class="product_preview" src="<!--{{img}}-->" alt="<!--{{name}}-->"/>
    <form class="order_section" data-price="<!--{{price}}-->" data-id="<!--{{id}}-->">
        <ul>
            <li>
                <p>Количество</p>
                <a href="" class="decrease product_quantity">
                    <img src="/modules/admin/assets/css/images/remove.png" alt=""/>
                </a>
                <input type="text" value="1" name="quantity" class="product_quantity_input"/>
                <span class="measurement"><!--{{measure_in}}--></span>
                <a href="" class="increase product_quantity">
                    <img src="/modules/admin/assets/css/images/add.png" alt=""/>
                </a>
            </li>
            <li>
                <p>Цена</p>
                <span class="price"><!--{{price}}--></span>
            </li>
        </ul>
        <input type="submit" value="В корзину"/>
        <a class="one_click" href="#">заказ в один клик</a>
    </form>
    <div class="product_description">
        <h3>Описание</h3>
        <!--{{text}}-->
        <h3>Технические характеристики</h3>
        <!--{{specs}}-->
    </div>
</div>