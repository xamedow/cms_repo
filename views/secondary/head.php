<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta charset="utf-8">
    <!--{{styles}}-->
    <meta name="keywords" content="<!--{{keys}}-->">
    <meta name="description" content="<!--{{description}}-->">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <title><!--{{title}}--></title>
    <!--{{scripts}}-->
</head>
<body>