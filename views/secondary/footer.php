<footer>
    <div id="footer_center">
        <ul id="footer_menu">
            <!--{{menu}}-->static<!--{{/menu}}-->
        </ul>
        <div class="info">«Астера» - интернет-магазин по продаже строительных материалов. Не является публичной
            офертой
        </div>
        <div class="phone">+7 (8452) <!--{{contact}}-->phone<!--{{/contact}}--></div>
        <a href="" class="callback call_me">заказать обратный звонок</a>

        <div class="question">Остались вопросы?</div>
        <a href="" class="consultant">напишите нам сейчас</a>

        <div class="copyright">
            Copyright &copy; 2012-2014 «Астера»<br>
            <a href="/">Строительные и отделочные материалы саратов</a>
        </div>
        <ul class="counter">
            <!--{{banner}}-->Счетчики<!--{{/banner}}-->
        </ul>
        <div class="developer">
            <a href="http://www.cherepkova.ru">Разработка сайта</a><br>
            Дизайн-студия Антонины Черепковой
        </div>
</footer>
</div>
<a href='#' id='Go_Top'  class="hide">НАВЕРХ</a>
</body>
</html>