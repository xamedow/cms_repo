$(function () {
    $('#scroll').tinycarousel();
    var catalog = document.getElementById('catalog'),
        catalogLi = catalog ? catalog.getElementsByTagName('ul')[0] : null,
        pull = $('#pull'),
        menu = $('nav ul'),
        menuHeight = menu.height(),
        loca = window.location.href,
        locaPage = loca.slice(loca.lastIndexOf('/')),
        addToCart = '';

// Catalog subsections folding

    $('#catalog_menu').find('a[href="/catalog' + locaPage + '"]').next().find('ul').show();

    catalogLi = catalogLi ? catalogLi.children : null;
    if (catalogLi) {
        for (var i = 0; i < catalogLi.length; i++) {
            var catName = catalogLi[i].getElementsByClassName('cat_name')[0];
            if (catName) {
                catName.innerHTML = catalogLi[i].getElementsByTagName('a')[0].innerHTML;
            }
        }
    }

    $(pull).on('click', function (e) {
        e.preventDefault();
        menu.slideToggle();
    });

    $(window).resize(function () {
        var w = $(window).width();
        if (w > 320 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
// Product quantity increase/decrease.
    function quantificate() {
        var inp, val,
            decrease = $('.decrease.product_quantity'),
            increase = $('.increase.product_quantity'),
            process = function (e) {
                e.preventDefault();
                if ($(this).hasClass('decrease') && val > 1) {
                    inp = $(this).next();
                    val = inp.val();
                    inp.val(--val);
                } else if ($(this).hasClass('increase')) {
                    inp = $(this).prev();
                    val = inp.val();
                    inp.val(++val);
                }

            };
        decrease.click(process);
        increase.click(process);

    }

    quantificate();
// Basket
    var basket = {
        summ: 0,
        items: [],

        getQuantity: function () {
            return this.items.length;
        }
    };
// Basket form.
    (function () {
        var form = $('form.order_section'),
            holder = $('#bucket'),
        //quantity = form.find('.product_quantity_input').val(),
            getId = function () {
                return parseInt(form.data('id'));
            },
            getQuantity = function () {
                return parseInt(form.find('.product_quantity_input').val());
            },
            getPrice = function () {
                return parseInt(form.data('price'));
            },
            getSum = function () {
                return getPrice() * getQuantity();
            },
            getEnding = function () {
                var products = getQuantity(),
                    lastNum = products.toString().slice(-1);

                if (lastNum > '4' || products == 11 || lastNum == 0) {
                    return 'ов';
                } else if (lastNum === '1') {
                    return '';
                } else {
                    return 'а';
                }
            },
            checkBasket = function () {
                if (!$('div.true_basket')[0]) {
                    holder.html(
                        '<div class="true_basket">' +
                        'В вашей корзине ' + getQuantity() + ' товар' + getEnding() + '<br>на сумму <span class="sum">' + getSum() +
                        ' рублей</span>' +
                        '<a href="#">Оформить заказ</a>' +
                        '</div>');
                }
            },
            sendToBasket = function (e) {
                e.preventDefault();
                checkBasket();
                $.ajax({
                    url: '',
                    type: 'post',
                    data: {
                        action: 'addToBasket',
                        id: getId(),
                        quantity: getQuantity(),
                        price: getPrice()
                    }
                }).done(function (data) {
                    alert('Товар добавлен в корзину.');
                    //console.log(data);
                });
            };

        form.submit(sendToBasket);
    })();

// Order form
    (function () {
        var trigger = $('div.true_basket a'),
            orders = $('.order_holder'),
            totalSum = 0,
            ordersHtml = orders.html(),
            removeProduct = function (e) {
                e.preventDefault();
                if (confirm('Удалить товар из корзины?')) {
                    var that = $(this);
                    $.ajax({
                        url: '',
                        type: 'post',
                        data: {
                            args: that.data('id'),
                            action: 'removeProduct'
                        }
                    }).done(function (data) {
                        that.parent().remove();
                        //console.log(data);
                    });
                }
            },
            makeOrder = function (data) {
                getBasketData(data);
            },
            makeOrderProductsHTML = function (data, basketData) {
                var out = '', i = 0, l = data.length, amount, price;
                if (data && data !== '0') {
                    for (; i < l; ++i) {
                        amount = makeQuantificator(basketData[data[i].id], data[i].measure_in);
                        price = basketData[data[i].id].quantity * data[i].price;
                        totalSum += price;
                        out += [
                            '<li><a href="',
                            getHref(data[i].name),
                            '">',
                            data[i].name,
                            '</a><span class="quantity">количество: ',
                            amount,
                            '</span><span class="price">Цена: ' + price + ' руб.</span>' +
                            '<a class="remove_product" href="#" data-id="' + data[i].id + '">удалить</a></li>'].join('');
                    }
                }
                return '<ul class="order_products">' + out;
            },

            makeQuantificator = function (amount, measure) {
                return ['<a href="" class="decrease product_quantity">',
                    '<img src="/modules/admin/assets/css/images/remove.png" alt="">',
                    '</a>',
                    '<input type="text" value="',
                    amount.quantity,
                    '" name="quantity" class="product_quantity_input">',
                    '<a href="" class="increase product_quantity">',
                    '<img src="/modules/admin/assets/css/images/add.png" alt="">',
                    '</a>',
                    '<strong class="measurement"> ',
                    measure,
                    '</strong>'].join('');
            },

            getDeliverySum = function () {
                return 300;
            },
            getSumHTML = function () {
                var deliverySum = getDeliverySum(),
                    sum = totalSum + deliverySum,
                    delivery = deliverySum
                        ? '<li><h4>Стоимость доставки</h4><p>' + deliverySum + ' руб.</p></li>'
                        : '';

                return '<li><a class="clear_basket" href="#">Очистить корзину</a></li>' +
                delivery + '<li><h4 class="sum">Итого</h4>' +
                '<p>' + sum + ' руб.</p></li>' +
                '<li><button>Оформить заказ</button></li>';
            },

            clearBasket = function () {
                if (confirm('Очистить корзину?')) {
                    $.ajax({
                        url: '',
                        type: 'post',
                        data: {
                            action: 'clearBasket'
                        }
                    }).done(function () {

                    });
                }
            },

            getHref = function () {
            },
            getBasketData = function (data) {
                $.ajax({
                    url: '',
                    type: 'post',
                    data: {
                        action: 'getBasket'
                    }
                }).done(function (basketData) {
                    var products = makeOrderProductsHTML(data, basketData);
                    orders.html(ordersHtml + products + getSumHTML());
                    $('a.remove_product').click(removeProduct);
                    quantificate();
                    orders.show('slow');

                });
            },
            getProducts = function () {
                $.ajax({
                    url: '',
                    type: 'post',
                    data: {
                        action: 'getProducts'
                    }
                }).done(function (data) {
                    makeOrder(data);
                });
            },
            order = function (e) {
                e.preventDefault();
                if (orders.css('display') === 'none') {
                    getProducts();
                }
            };

        trigger.click(order);
    })();

// Reg form
    (function () {
        var form = $('form.reg_init');
        form.submit(function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: '',
                data: {
                    action: 'newReg',
                    table: 'ref_clients',
                    name: form.find('[name="new_user_name"]').val(),
                    login: form.find('[name="new_user_email"]').val(),
                    phone: form.find('[name="new_user_phone"]').val(),
                    address: form.find('[name="new_user_address"]').val(),
                    captcha: form.find('[name="new_user_captcha"]').val(),
                    pass: form.find('[name="new_user_pass"]').val()
                }
            }).done(function (data) {
                alert(data);
                //window.location.reload();
            });
        });
    })();

// Tabs
    $("#tabs").tabs();

// Order List
    (function () {
        var constructOrder = function (data) {
                        var productsData = JSON.parse(data),
                            i = 0, l = productsData.length, out = '', row, sum = 0;
                        for (; i < l; ++i) {
                            row = productsData[i];
                            out += '<li><a href="/catalog/' + row.id + '.html">' + row.name + ' - ' + row.quantity +
                            ' ' + row.measure_in + '<span class="order_price"> ' +
                            (row.price * row.quantity) + ' руб.</span>' + '</a>';
                            sum += row.price * row.quantity;
                        }
                        out = '<ul>' + out + '<p>Итого: '+ sum + ' руб.</p></ul>';

                    this.pointer.parent().html(this.pointer.parent().html() + out);
            },

            getOrder = function (e) {
                e.preventDefault();
                var that = $(this);
                    $.ajax({
                        type: 'post',
                        url: '',
                        pointer: that,
                        data: {
                            action: 'getorder',
                            id: that.data('id')
                        }
                    }).done(constructOrder);
            };
            $('ul.order_list>li>a').click(getOrder);
    })();

// Logout
    (function () {
        $('a.logout').click(function () {
            $.ajax({
                type: 'post',
                url: '',
                data: {
                    action: 'logout'
                }
            }).done(function () {
                window.location.reload();
            });
        });
    })();
// Go Top button
    if (document.body.clientWidth <= 600) {
        $.fn.scrollToTop = function () {
            $(this).hide().removeAttr("href");
            if ($(window).scrollTop() >= "300") $(this).fadeIn("fast")
            var scrollDiv = $(this);
            $(window).scroll(function () {
                if ($(window).scrollTop() <= "300") $(scrollDiv).fadeOut("fast")
                else $(scrollDiv).fadeIn("fast")
            });
            $(this).click(function () {
                $("html, body").animate({scrollTop: 0}, "fast")
            })
        };

        $("#Go_Top").scrollToTop();
    }

// Search autocomplete.
    (function () {
        var cache = {};
        $("#search_text").autocomplete({
            minLength: 2,
            source: function (request, response) {
                var term = request.term;
                if (term in cache) {
                    response(cache[term]);
                    return;
                }

                $.getJSON("", request, function (data, status, xhr) {
                    cache[term] = data;
                    response(data);
                });
            }
        });
    })();
});