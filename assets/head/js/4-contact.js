$(function ($) {
    'use strict';
    var contact = {
        message: null,
        init: function () {
            $('a.call_me').click(function (e) {
                e.preventDefault();

                // load the contact form using ajax
                $.get("/plugins/contact.php", function (data) {
                    // create a modal dialog with the data
                    $(data).modal({
                        closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
                        position: ["15%"],
                        overlayId: 'contact-overlay',
                        containerId: 'contact-container',
                        onOpen: contact.open,
                        onShow: contact.show,
                        onClose: contact.close
                    });
                });
            });
        },
        open: function (dialog) {

            // dynamically determine height
            var h = 350;
            if ($('#contact-subject').length) {
                h += 26;
            }
            if ($('#contact-cc').length) {
                h += 22;
            }

            var title = $('#contact-container .contact-title').html();
            $('#contact-container .contact-title').html('Ждите...');
            dialog.overlay.fadeIn(200, function () {
                dialog.container.fadeIn(200, function () {
                    dialog.data.fadeIn(200, function () {
                        $('#contact-container .contact-content').animate({
                            height: h
                        }, function () {
                            $('#contact-container .contact-title').html(title);
                            $('#contact-container form').fadeIn(200, function () {
                                $('#contact-container #contact-name').focus();

                                $('#contact-container .contact-cc').click(function () {
                                    var cc = $('#contact-container #contact-cc');
                                    cc.is(':checked') ? cc.attr('checked', '') : cc.attr('checked', 'checked');
                                });
                            });
                        });
                    });
                });
            });
        },
        show: function (dialog) {
            $('#contact-container .contact-send').click(function (e) {
                e.preventDefault();
                // validate form
                if (contact.validate()) {
                    var msg = $('#contact-container .contact-message');
                    msg.fadeOut(function () {
                        msg.removeClass('contact-error').empty();
                    });
                    $('#contact-container .contact-title').html('Отправляем...');
                    $('#contact-container form').fadeOut(200);
                    $('#contact-container .contact-content').animate({
                        height: '80px'
                    }, function () {
                        $('#contact-container .contact-loading').fadeIn(200, function () {
                            $.ajax({
                                url: '/plugins\/contact.php',
                                data: $('#contact-container form').serialize() + '&action=send',
                                type: 'post',
                                cache: false,
                                dataType: 'html',
                                success: function (data) {
                                    $('#contact-container .contact-loading').fadeOut(200, function () {
                                        $('#contact-container .contact-title').html('Спасибо!<br>');
                                        msg.html(data).fadeIn(200);
                                    });
                                },
                                error: contact.error
                            });
                        });
                    });
                } else {
                    if ($('#contact-container .contact-message:visible').length > 0) {
                        var msg = $('#contact-container .contact-message div');
                        msg.fadeOut(200, function () {
                            msg.empty();
                            contact.showError();
                            msg.fadeIn(200);
                        });
                    } else {
                        $('#contact-container .contact-message').animate({
                            height: '30px'
                        }, contact.showError);
                    }

                }
            });
        },
        close: function (dialog) {
            $('#contact-container .contact-message').fadeOut();
            $('#contact-container .contact-title').html('До свидания...');
            $('#contact-container form').fadeOut(200);
            $('#contact-container .contact-content').animate({
                height: 40
            }, function () {
                dialog.data.fadeOut(200, function () {
                    dialog.container.fadeOut(200, function () {
                        dialog.overlay.fadeOut(200, function () {
                            $.modal.close();
                        });
                    });
                });
            });
        },
        error: function (xhr) {
            alert(xhr.statusText);
            $.modal.close();
        },
        validate: function () {
            contact.message = '';
            if (!$('#contact-container #contact-name').val()) {
                contact.message += 'Введите имя. ';
            }

            
            if (contact.message.length > 0) {
                return false;
            }
            else {
                return true;
            }
        },
        validateEmail: function (email) {
            var at = email.lastIndexOf("@");

            // Make sure the at (@) sybmol exists and
            // it is not the first or last character
            if (at < 1 || (at + 1) === email.length)
                return false;

            // Make sure there aren't multiple periods together
            if (/(\.{2,})/.test(email))
                return false;

            // Break up the local and domain portions
            var local = email.substring(0, at);
            var domain = email.substring(at + 1);

            // Check lengths
            if (local.length < 1 || local.length > 64 || domain.length < 4 || domain.length > 255)
                return false;

            // Make sure local and domain don't start with or end with a period
            if (/(^\.|\.$)/.test(local) || /(^\.|\.$)/.test(domain))
                return false;

            // Check for quoted-string addresses
            // Since almost anything is allowed in a quoted-string address,
            // we're just going to let them go through
            if (!/^"(.+)"$/.test(local)) {
                // It's a dot-string address...check for valid characters
                if (!/^[-a-zA-Z0-9!#$%*\/?|^{}`~&'+=_\.]*$/.test(local))
                    return false;
            }

            // Make sure domain contains only valid characters and at least one period
            if (!/^[-a-zA-Z0-9\.]*$/.test(domain) || domain.indexOf(".") === -1)
                return false;

            return true;
        },
        showError: function () {
            $('#contact-container .contact-message')
                .html($('<div class="contact-error"></div>').append(contact.message))
                .fadeIn(200);
        }
    };
    var contact2 = {
        message: null,
        init: function () {
            $('a.review').click(function (e) {
                e.preventDefault();

                // load the contact form using ajax
                $.get("/plugins/contact2.php", function (data) {
                    // create a modal dialog with the data
                    $(data).modal({
                        closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
                        position: ["15%"],
                        overlayId: 'contact-overlay',
                        containerId: 'contact-container',
                        onOpen: contact.open,
                        onShow: contact.show,
                        onClose: contact.close
                    });
                });
            });
        },
        open: function (dialog) {

            // dynamically determine height
            var h = 330;
            if ($('#contact-subject').length) {
                h += 26;
            }
            if ($('#contact-cc').length) {
                h += 22;
            }

            var title = $('#contact-container .contact-title').html();
            $('#contact-container .contact-title').html('Ждите...');
            dialog.overlay.fadeIn(200, function () {
                dialog.container.fadeIn(200, function () {
                    dialog.data.fadeIn(200, function () {
                        $('#contact-container .contact-content').animate({
                            height: h
                        }, function () {
                            $('#contact-container .contact-title').html(title);
                            $('#contact-container form').fadeIn(200, function () {
                                $('#contact-container #contact-name').focus();

                                $('#contact-container .contact-cc').click(function () {
                                    var cc = $('#contact-container #contact-cc');
                                    cc.is(':checked') ? cc.attr('checked', '') : cc.attr('checked', 'checked');
                                });
                            });
                        });
                    });
                });
            });
        },
        show: function (dialog) {
            $('#contact-container .contact-send').click(function (e) {
                e.preventDefault();
                // validate form
                if (contact.validate()) {
                    var msg = $('#contact-container .contact-message');
                    msg.fadeOut(function () {
                        msg.removeClass('contact-error').empty();
                    });
                    $('#contact-container .contact-title').html('Отправляем...');
                    $('#contact-container form').fadeOut(200);
                    $('#contact-container .contact-content').animate({
                        height: '80px'
                    }, function () {
                        $('#contact-container .contact-loading').fadeIn(200, function () {
                            $.ajax({
                                url: '/plugins\/contact.php',
                                data: $('#contact-container form').serialize() + '&action=send',
                                type: 'post',
                                cache: false,
                                dataType: 'html',
                                success: function (data) {
                                    $('#contact-container .contact-loading').fadeOut(200, function () {
                                        $('#contact-container .contact-title').html('Спасибо!');
                                        msg.html(data).fadeIn(200);
                                    });
                                },
                                error: contact.error
                            });
                        });
                    });
                } else {
                    if ($('#contact-container .contact-message:visible').length > 0) {
                        var msg = $('#contact-container .contact-message div');
                        msg.fadeOut(200, function () {
                            msg.empty();
                            contact.showError();
                            msg.fadeIn(200);
                        });
                    } else {
                        $('#contact-container .contact-message').animate({
                            height: '30px'
                        }, contact.showError);
                    }

                }
            });
        },
        close: function (dialog) {
            $('#contact-container .contact-message').fadeOut();
            $('#contact-container .contact-title').html('До свидания...');
            $('#contact-container form').fadeOut(200);
            $('#contact-container .contact-content').animate({
                height: 40
            }, function () {
                dialog.data.fadeOut(200, function () {
                    dialog.container.fadeOut(200, function () {
                        dialog.overlay.fadeOut(200, function () {
                            $.modal.close();
                        });
                    });
                });
            });
        },
        error: function (xhr) {
            alert(xhr.statusText);
            $.modal.close();
        },
        validate: function () {
            contact.message = '';
            if (!$('#contact-container #contact-name').val()) {
                contact.message += 'Введите имя. ';
            }

            if (!$('#contact-container #contact-message').val()) {
                contact.message += 'Введите телефон.';
            }
            if (contact.message.length > 0) {
                return false;
            }
            else {
                return true;
            }
        },
        validateEmail: function (email) {
            var at = email.lastIndexOf("@");

            // Make sure the at (@) sybmol exists and
            // it is not the first or last character
            if (at < 1 || (at + 1) === email.length)
                return false;

            // Make sure there aren't multiple periods together
            if (/(\.{2,})/.test(email))
                return false;

            // Break up the local and domain portions
            var local = email.substring(0, at);
            var domain = email.substring(at + 1);

            // Check lengths
            if (local.length < 1 || local.length > 64 || domain.length < 4 || domain.length > 255)
                return false;

            // Make sure local and domain don't start with or end with a period
            if (/(^\.|\.$)/.test(local) || /(^\.|\.$)/.test(domain))
                return false;

            // Check for quoted-string addresses
            // Since almost anything is allowed in a quoted-string address,
            // we're just going to let them go through
            if (!/^"(.+)"$/.test(local)) {
                // It's a dot-string address...check for valid characters
                if (!/^[-a-zA-Z0-9!#$%*\/?|^{}`~&'+=_\.]*$/.test(local))
                    return false;
            }

            // Make sure domain contains only valid characters and at least one period
            if (!/^[-a-zA-Z0-9\.]*$/.test(domain) || domain.indexOf(".") === -1)
                return false;

            return true;
        },
        showError: function () {
            $('#contact-container .contact-message')
                .html($('<div class="contact-error"></div>').append(contact.message))
                .fadeIn(200);
        }
    };

    contact.init();
    contact2.init();

});