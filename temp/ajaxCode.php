<?php
/**
 * Date: 17.07.14
 * Time: 0:52
 */
// TODO emphasize this to distinct class.
// TODO filter _POST.
if (!empty($_POST) && isset($_POST['action'])) {
    $table = isset($_POST['table']) ? $_POST['table'] : '';
    $skipedItems = array(
        'action',
        'table',
        'submit',
        'pass_change',
        'id'
    );
    // Сохранение

    if(!empty($_FILES)) {
        $writer = new \modules\admin\ImageWriter(array('sm'=>array(455,280), 'bg'=>array(980,735)));
    }
    if ($_POST['action'] === 'new') {
        $values = $keys = '';
        $insert_arr = array();

        $rank = current(db::queryExec("SELECT MAX(rank) as max FROM $table"));
        $rank = $rank['max'] + 1;
        if(array_key_exists('rank', $_POST) === false) {
            $insert_arr[':rank'] = $rank;
            $keys .= 'rank,';
            $values .= ':rank,';
        }
        foreach ($_POST as $key => $val) {
            if (!in_array($key, $skipedItems)) {
                $val = $key === 'rank' ? $rank : $val;
                $insert_arr[':' . $key] = $val;
                $keys .= "$key,";
                $values .= ":$key,";
            }
        }
        $keys = rtrim($keys, " \n\t,");
        $values = rtrim($values, " \n\t,");
        db::queryExec("INSERT INTO $table ($keys) VALUES ($values)", $insert_arr, false);
    }
    if ($_POST['action'] === 'save') {
        if (r::getPageName() !== 'contacts') {
            foreach ($_POST as $key => $val) {
                if (!in_array($key, $skipedItems)) {
                    if ($key === 'pass' && !empty($val) && !empty($_POST['pass_change'])) {
                        $values .= "pass='" . substr(sha1(trim($val)), 4, -4) . "',";
                    } elseif ($key !== 'pass') {
                        $values .= "$key='$val',";
                    }
                }
            }
            db::queryExec("UPDATE $table SET " . rtrim($values, " \n\t,") . " WHERE rank={$_POST['rank']}", array(), false);
        } else {
            $matches = array();
            foreach ($_POST as $key => $val) {
                if (!in_array($key, $skipedItems)) {
                    preg_match('/([\w]+)([\d]+)$/', $key, $matches);
                    // TODO refactor this!!!!
                    db::queryExec("UPDATE $table SET {$matches[1]}=:val WHERE id=:id", array(':val' => $val, ':id' => $matches[2]), false);
                }
            }
        }
    }
    // Активность
    if ($_POST['action'] === 'act_change' || $_POST['action'] === 'del_change') {
        $field = $_POST['action'] === 'act_change' ? 'act' : 'trash';
        $values = explode(',', $_POST['id']);
        $placeholders = rtrim(str_repeat('?, ', count($values)), ', ');
        array_unshift($values, $_POST['state']);
        db::queryExec("UPDATE {$_POST['module']} SET $field = ? WHERE id IN ($placeholders)", $values, false);
    }
    // Выход
    if ($_POST['action'] === 'exit') {
        session_destroy();
    }
}